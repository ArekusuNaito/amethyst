﻿namespace Amethyst
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Virtues;
    using Virtues.Input;
    using Virtues.Services;
    using UniRx.Triggers;
    using UniRx;

    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class Enemy : MonoBehaviour
    {
        public ParticleSystem destroyedParticles;
        private Transform playerTransform;
        public float speed=3;
        
        public CardinalPoints.Direction facing = CardinalPoints.Down;
        public float bulletInterval=1;
        Vector2 velocity;

        BoxCollider2D boxCollider;
        public GameObject bullet;
        public bool canFire=false;
        public bool canMove=true;
        void Awake()
        {
            this.boxCollider = GetComponent<BoxCollider2D>();
            var playerReference = GameObject.FindObjectOfType<Player>();
            if (playerReference == null) return;
            this.playerTransform = playerReference.transform;
            
            //Observables
            if(canMove)
            {
                switch(Random.Range(0,2))
                {
                    case 0: BehaviourService.AddFollowMovement(this.gameObject, playerTransform, Random.Range(3f, 8f));break;
                    case 1: BehaviourService.AddHopperMovement(this.gameObject,Random.Range(4,9));break;
                }
                
            }
            
            //On Collision, Destroy or hit player
            this.OnCollisionEnter2DAsObservable().Subscribe(collision=>
            {
                if(collision.transform.HasComponent<Player>())
                {
                    var player = collision.transform.GetComponent<Player>();
                    if(player.isDashing)
                    {
                        var particles = ParticlesService.Create(destroyedParticles, this.transform);
                        CameraService.Shake(0.1f);
                        Destroy(this.gameObject);
                        //score up
                        HUD.score.Value++;
                    }
                    else player.TakeDamage();
                }
            }).AddTo(this);

            if(canFire)
            {
                //Shooting system for enemy
                Observable.Interval(System.TimeSpan.FromSeconds(bulletInterval)).Subscribe(_ =>
                {

                    var position = CardinalPoints.DirectionToBoundsPosition(this.facing, this.boxCollider.bounds);
                    var bulletClone = Instantiate(bullet, position, Quaternion.identity).GetComponent<Projectile>();
                    bulletClone.Initialize(this.facing);
                }).AddTo(this);
            }
            
            
        }

        public float angle;
        void Update()
        {
            if(playerTransform!=null)
            {
                var direction = (playerTransform.position - this.transform.position);
                angle = Vector2.SignedAngle(direction, Vector2.up); //Use of Vector.up to make the right side positive (0,180) and the left (-180,0)
                angle = CardinalPoints.From180AngleTo360AngleInDegrees(angle);
                var newFacing = CardinalPoints.DegreeAngleTo4Direction(angle);
                if(newFacing!=this.facing)
                {
                    this.facing=newFacing;
                    GetComponent<Animator>().SetInteger("Facing",(int)this.facing.value);
                }
                
                
            }

        }

    }

}