﻿namespace Amethyst
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using Virtues;
    using Virtues.UI;
    using UniRx;
    using UniRx.Async;
    using UnityEngine.SceneManagement;

    public class HUD : MonoBehaviour
    {
        [Header("Life")]
        public RectTransform lifeContainer;
        public Sprite fullHeartSprite,damagedHeartSprite;
        [Header("Score")]
        public Text scoreText;
        public GameObject gameOverPanel;
        public static IntReactiveProperty score = new IntReactiveProperty(0);
        public Vector3 originalCameraPosition;
        
        void Awake()
        {
            this.originalCameraPosition = Camera.main.transform.position;
            Observable.Interval(System.TimeSpan.FromSeconds(5f)).Subscribe(_=>Camera.main.transform.position=originalCameraPosition).AddTo(this);
            //Life
            var player =GameObject.FindObjectOfType<Player>();
            player.life.Subscribe(life=>
            {
                if(life>0) //Add animation
                {
                    var heart = lifeContainer.GetChild(life - 1);
                }
                else //Game Over
                {
                    gameOverPanel.SetActive(true);
                    var scoreObjectFromPanel = gameOverPanel.transform.GetChild(0);
                    scoreObjectFromPanel.GetComponent<Text>().text=$"SCORE\n{score.Value}";
                    var restartMessage = gameOverPanel.transform.GetChild(1); //hardcoded to get 2nd child, which is the pressAnyKey
                    //Add Animation
                    Observable.Interval(System.TimeSpan.FromSeconds(0.5f)).Subscribe(intervalCounter=>
                    {
                        restartMessage.gameObject.SetActive(!restartMessage.gameObject.activeSelf);
                    }).AddTo(this);
                    //Restart input
                    var restartInput = Observable.EveryUpdate().Where(_=>Input.GetButtonDown("Submit")).Take(1);
                    restartInput.Subscribe(async _=>
                    {
                        score.Value=0;
                        await SceneManager.LoadSceneAsync(0).ConfigureAwait();
                    }).AddTo(this);
                    
                }
                Debug.Log(life);
                SetLife(life);
            }).AddTo(player);
            //Score
            score.SubscribeToText(this.scoreText).AddTo(this);
        }

        public void SetLife(int life)
        {
            var maxLife=3;//lol remove this, this has to be inside the player
            for(var goodIndex=0;goodIndex<life;goodIndex++)
            {
                lifeContainer.GetChild(goodIndex).GetComponent<Image>().sprite=fullHeartSprite;
            }
            for(var badIndex=maxLife-1;badIndex>=life;badIndex--)
            {
                lifeContainer.GetChild(badIndex).GetComponent<Image>().sprite = damagedHeartSprite;
            }
            
        }


    }

}