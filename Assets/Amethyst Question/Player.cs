﻿namespace Amethyst
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Virtues;
    using Virtues.Audio;
    using Virtues.Input;
    using Virtues.Services;
    using UniRx;

    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    // [RequireComponent(typeof(Rigidbody2D))]
    public class Player : MonoBehaviour
    {
        
        [Range(1,10)]
        public float xSpeed=2;
        [Range(1, 10)]
        public float ySpeed=2;
        private BoxCollider2D boxCollider;
        private Rigidbody2D body;
        public LayerMask collisionLayers;
        public LayerMask destructableCollisionLayer;
        ContactFilter2D contactFilter;
        private Vector2 velocity;
        public CardinalPoints.Direction facingAt = CardinalPoints.Up;
        public bool isDashing = false;
        private SpriteRenderer spriteRenderer;
        public IntReactiveProperty life= new IntReactiveProperty(3);
        [RangeReactiveProperty(0.3f,2)]
        public FloatReactiveProperty invincibilityTime = new FloatReactiveProperty(0.3f);
        public bool isInvincible=false;

        
        
        Vector2 deleteMeDistance = Vector2.zero;
        void Awake()
        {
            this.spriteRenderer = GetComponent<SpriteRenderer>();
            this.boxCollider = this.GetComponent<BoxCollider2D>();
            this.body = this.GetComponent<Rigidbody2D>();
            contactFilter.useTriggers=false;
            contactFilter.SetLayerMask(this.collisionLayers);
            
        }


        void Update()
        {
            var leftStick = Gamepad.LeftStick;
            if(!isDashing)
            {
                this.velocity = new Vector2(leftStick.x * xSpeed, leftStick.y * ySpeed);
                if(Gamepad.Left)this.facingAt=CardinalPoints.Left;
                else if (Gamepad.Right) this.facingAt = CardinalPoints.Right;
                else if (Gamepad.Up) this.facingAt = CardinalPoints.Up;
                else if (Gamepad.Down) this.facingAt = CardinalPoints.Down;
                

            }
            if (Input.GetKeyDown(KeyCode.Space) &&!isDashing)
            {

                if (this.facingAt == CardinalPoints.Up || this.facingAt == CardinalPoints.Down) this.velocity = this.facingAt.vector * ySpeed*3;
                if (this.facingAt == CardinalPoints.Left || this.facingAt == CardinalPoints.Right) this.velocity = this.facingAt.vector * xSpeed*3;
                Debug.Log($"{this.velocity}");
                isDashing=true;
                Observable.Timer(System.TimeSpan.FromSeconds(0.1f)).Subscribe(_=>isDashing=false);
            }
        }

        public void TakeDamage()
        {
            if(!isInvincible)
            {
                isInvincible=true;
                //SFX
                life.Value--;
                CameraService.Shake(0.15f);
                Observable.Timer(System.TimeSpan.FromSeconds(invincibilityTime.Value)).Subscribe(_ => isInvincible = false);
                if (life.Value > 0)//are you still alive?
                {
                    
                    Observable.Interval(System.TimeSpan.FromSeconds(0.05f)).Take((int)(invincibilityTime.Value / 0.05f)).Subscribe(counter =>
                    {
                        
                        if (counter % 2 == 0) this.spriteRenderer.enabled = false;
                        else this.spriteRenderer.enabled = true;
                    }, 
                    () => //onComplete
                    {
                        this.spriteRenderer.enabled = true;

                    }).AddTo(this);
                }
                else 
                {
                    Destroy(this.gameObject);
                } //well, then dissapear from existence
            }
        }


        // Update is called once per frame
        void FixedUpdate()
        {
            
            var leftStick = Gamepad.LeftStick;
            if(!isDashing)this.velocity = new Vector2(leftStick.x * xSpeed, leftStick.y * ySpeed);
            var distanceInOneFrame = PhysicsService.DistanceInOneFrame(this.velocity);
            Debug.DrawRay(this.boxCollider.bounds.center, distanceInOneFrame,Color.cyan);
            CheckWallCollision(ref distanceInOneFrame);

            if(distanceInOneFrame!=Vector2.zero)this.transform.Translate(distanceInOneFrame);
        }

        void CheckWallCollision(ref Vector2 distanceInOneFrame)
        {
            var hits = CollisionService.Detect(this.boxCollider, contactFilter, distanceInOneFrame);
            if (hits != null)
            {
                CollisionService.RepositionCollider(this.boxCollider, hits, ref distanceInOneFrame);
            }
        }
        
    }

}