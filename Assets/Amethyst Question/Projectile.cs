﻿namespace Amethyst
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Virtues;

    public class Projectile : MonoBehaviour
    {
        private Rigidbody2D body;
        public float speed=4f;
        void Awake()
        {
            body = GetComponent<Rigidbody2D>();
        }
        public void Initialize(CardinalPoints.Direction direction)
        {
            this.transform.Rotate(0, 0, (float)direction.value * 90);
            body.velocity = direction.vector*speed;
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.transform.HasComponent<Player>())
            {
                var player = collision.transform.GetComponent<Player>();
                player.TakeDamage();
            }
        }

    }

}