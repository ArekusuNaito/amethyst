﻿namespace Amethyst
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Virtues;
    using UniRx;
    public class Spawner : MonoBehaviour
    {
        public List<GameObject> enemyList = new List<GameObject>();
        public float spawnInterval = 0;
        void Awake()
        {
            Observable.Interval(TimeSpan.FromSeconds(spawnInterval)).Subscribe(_=>
            {
                var randomIndex = UnityEngine.Random.Range(0, enemyList.Count);
                var chosenEnemy = enemyList[randomIndex];
                Instantiate(chosenEnemy,this.transform.position,Quaternion.identity);
            }).AddTo(this.gameObject);
        }
    }

}