﻿namespace MechanicsWizard
{

    using System.Collections.Generic;
    using Virtues;
    using UnityEngine;
    using UnityEngine.UI;
    using System.Linq;
    using UnityEngine.EventSystems;
    using UnityEngine.Events;
    public class AddCursorSelectionToSelectables : MonoBehaviour
    {
        public List<GameObject> objectsWithSelectables;
        public float cursorXOffset = 0.5f;
        public RectTransform  cursor;
        public GameObject defaultSelectedObject;
        ///If you want to add it to specific items
        // public List<Selectable> selectables;
        
        public void SelectDefault()
        {
            EventSystem.current.SetSelectedGameObject(this.defaultSelectedObject);
        }
        EventTrigger.Entry CreateOnSelect(UnityAction<BaseEventData> callBack)
        {  
            var onSelect = new EventTrigger.Entry();
            onSelect.eventID = EventTriggerType.Select;
            onSelect.callback.AddListener(callBack);
            return onSelect;
        }

        public void AddCursorToSelectables()
        {
            if(objectsWithSelectables.Count>0)
            {
                this.objectsWithSelectables.ForEach(fatherComponent=>
                {
                    //Father is not considered to be a selectable
                    var selectables = fatherComponent.GetComponentsInChildren<Selectable>().ToList();
                    selectables.ForEach(selectable=>
                    {
                        //Create an OnSelect Trigger
                        var eventTrigger = selectable.gameObject.AddComponent<EventTrigger>();
                        // When selected, the cursor will be placed to the left of the elements
                        eventTrigger.triggers.Add(CreateOnSelect(data=>
                        {
                                var selectedObject = data.selectedObject;
                                var selected = selectedObject.transform as RectTransform;
                                
                                var cursorX = (selected.position.x - this.cursorXOffset) -  (selected.GetSizeInWorldUnits().x - this.cursor.GetSizeInWorldUnits().x);
                                this.cursor.position = new Vector2(cursorX,selected.position.y);
                                // Debug.Log($"Selected: {selectable.name} {selected.position.y}");
                                // if(this.cursor.HasComponent<CursorAnimation>())
                                // {
                                //     var animation = this.cursor.GetComponent<CursorAnimation>();
                                //     animation.Restart();
                                // }
                                
                        }));
                    });
                });
            }
            else
            {
                Debug.LogWarning($"Selectables list is empty");
            }
        }

        
        void Start()
        {   
            
            this.AddCursorToSelectables();
            if(defaultSelectedObject.HasComponent<Selectable>())
            {
                EventSystem.current.SetSelectedGameObject(defaultSelectedObject);
                SelectDefault();
            }else throw new System.Exception("Default Object does not have a Selectable Component");
            
        }

    }

}