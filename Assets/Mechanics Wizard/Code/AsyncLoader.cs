﻿namespace MechanicsWizard
{   
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using DG.Tweening;
    using UnityEngine;
    using Virtues.Audio;

public class AsyncLoader : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioPlayer player;
    public Virtues.Scene.SceneChanger sceneChanger;
    public Transition transition;
    public bool enableAutoFadeIn=true;
    void Start()
    {
        player.Initialize();
        if(transition!=null && enableAutoFadeIn)
        {
            transition.SetAlpha(1);
            transition.FadeIn();
            enableAutoFadeIn=true;
        }
    }

    async public void FadeOutScene(int sceneIndex)
    {
        if(transition!=null)
        {
            transition.FadeOut(async ()=>
            {
                await sceneChanger.ChangeScene(sceneIndex);
                    
            });
        }
        else
        {
            await sceneChanger.ChangeScene(sceneIndex);
        }
        
    }

    async public void FadeInScene(int sceneIndex)
    {
        if(transition!=null)
        {
            transition.FadeIn(async ()=>
            {
                await sceneChanger.ChangeScene(sceneIndex);
                    
            });
        }
        else
        {
            await sceneChanger.ChangeScene(sceneIndex);
        }
    }
}

}