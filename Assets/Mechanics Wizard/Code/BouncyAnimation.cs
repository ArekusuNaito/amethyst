﻿namespace MechanicsWizard
{
    using DG.Tweening;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class BouncyAnimation : MonoBehaviour
    {
        [Range(0,5)]
        public float durationInSeconds=1;
        [Range(-30,30)]
        public float yOffset = 1;
        // Start is called before the first frame update
        void Start()
        {
            this.transform.DOLocalMoveY(this.transform.localPosition.y+yOffset,durationInSeconds).SetEase(Ease.InOutFlash).SetLoops(-1,LoopType.Yoyo);
        }

        // Update is called once per frame
        void Update()
        {
            
        }
    }

}