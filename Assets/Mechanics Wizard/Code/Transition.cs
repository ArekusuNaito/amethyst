﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniRx.Async;
namespace MechanicsWizard
{
    public class Transition : MonoBehaviour
    {
        private float width;
        private RectTransform rectTransform;
        [SerializeField] CanvasGroup canvasGroup = null;
        public float transitionTime=1;
        void Start()
        {
            this.rectTransform = GetComponent<RectTransform>();
            this.width = rectTransform.rect.width;
            
            
        }

        public void SetAlpha(float alpha)
        {
            this.canvasGroup.alpha=alpha;
        }

        /// <summary>
        /// Fadeout: From Normal Image => Total Black
        /// This is; From Alpha 0 to 1; Since we have a black screen, we want to show the black screen.
        /// </summary>
        /// <param name="OnComplete"></param>
        public void FadeOut(TweenCallback OnComplete=null)
        {
            canvasGroup.DOFade(1,transitionTime).OnComplete(()=>
            {
                OnComplete();   
            }).SetUpdate(true);
        }
         /// <summary>
        /// FadeIn: From TotalBlack => Normal Image
        /// This is; From Alpha 1 to 0; We want to hide the black screen.
        /// </summary>
        /// <param name="OnComplete"></param>
        public void FadeIn(TweenCallback OnComplete=null)
        {
            canvasGroup.DOFade(0,transitionTime).OnComplete(()=>
            {
                OnComplete();
            }).SetUpdate(true);
        }
    }

}