﻿namespace Fireplace.Models
{
    using Models;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using System.Linq;

    public class Battlefield 
    {
        public List<Monster> monsters = new List<Monster>();   

        public Monster Summon(CardData card,Player owner)
        {
            var monster = new Monster(card,owner);
            this.monsters.Add(monster);
            return monster;
        }

        public Monster BestAttackMonster
        {
            get
            {   

                return (from monster in monsters
                let maxAttack = monsters.Max(monsterToken=>monsterToken.attack.Value)
                where monster.attack.Value==maxAttack
                select monster).FirstOrDefault();
            }
        }

        public Monster BestHealthMonster
        {
            get{
                return (from monster in monsters
                        let maxHealth = monsters.Max(monsterToken => monsterToken.health.Value)
                        where monster.health.Value == maxHealth
                        select monster).FirstOrDefault();
            }
        }

        

        public bool HasMonsters
        {
            get{return this.monsters.Count>0;}
        }

        public void Destroy(Monster monster)
        {
            monsters.Remove(monster);
        }

        public void EnableAttackForAllMonsters()
        {
            monsters.ForEach(monster=>monster.EnableAttack());
        }

        public void DisableAttackForAllMonsters()
        {
            monsters.ForEach(monster => monster.DisableAttack());
        }


    }

}