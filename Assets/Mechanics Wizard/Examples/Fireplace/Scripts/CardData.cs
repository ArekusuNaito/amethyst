﻿namespace Fireplace.Models
{
    using UnityEngine;
    [CreateAssetMenu(fileName = "Card Data", menuName = "Fireplace/New Card", order = 1)]
    [System.Serializable]
    public class CardData : ScriptableObject
    {
        public int cost  = 1;
        public float attack = 1;
        public float health = 1;
        
    }
}