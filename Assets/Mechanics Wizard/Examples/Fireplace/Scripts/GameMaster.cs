﻿namespace Fireplace
{
    using Virtues;
    using Virtues.Input;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using UnityEngine;
    using Models;
    using AI;
    using UI;
    using UniRx;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;

    public class GameMaster  : MonoBehaviour
    {
        
        public Store store;
        public Player player1,player2;
        public PlayerAI playerAI;

        public int HandSize = 3;
        public int InitialNumberOfCards=2;
        public int MaxNumberOfMonsters = 20;
        List<GameObject> handPoolUI = new List<GameObject>();
        ReactiveCollection<GameObject> monsterPoolUI = new ReactiveCollection<GameObject>();
        public GraphicComponents graphicComponents;

        public enum State { ChoosingEnemy, Idle};
        public UI.Monster source;
        
        [System.Serializable]
        public class Store
        {
            public Store(Player player1, Player player2)
            {
                this.turnOrder = new TurnOrder(player2,player1);
            }
            TurnOrder turnOrder;
            public Player NextPlayer{get{return this.turnOrder.NextPlayer;}}
            public Player CurrentPlayer { get { return this.turnOrder.CurrentPlayer; } }

            public State state = State.Idle;

            private bool canCurrentPlayerAttack = true;
            public bool CanCurrentPlayerAttack{get{return this.canCurrentPlayerAttack;}}
            public void DisablePlayerAttack()
            {
                this.canCurrentPlayerAttack=false;
            }
            public void PrepareNextTurn()
            {
                this.canCurrentPlayerAttack=true;
                NextPlayer.RestoreEnergy();
                NextPlayer.DrawCard();
            }

        }

        void Start()
        {
            this.store = new Store(player1,player2);
            this.playerAI = new PlayerAI(ref player2,ref player1,ref store);
            this.playerAI.SummonUI = this.SummonInField;
            // Force board enemy board
            // Show UI
            // foreach (var card in player1.deck.cards)
            // {

            //     SummonInField(player1, player1.Summon(card));
            // }
            // Debug.Log("Player 2");
            // foreach (var card in player2.deck.cards)
            // {
            //     SummonInField(player2, player2.Summon(card));
            // }

            // playerAI.Play();

            //Link Energy Logic to UI
            player1.currentEnergy.SubscribeToText(this.graphicComponents.playerEnergy,newValue=>$"{newValue.ToString()}/{player1.maxEnergy.Value.ToString()}");
            

            Action<int> CreateMonsterPool = (maxNumberOfMonsters)=>
            {
                for (int index = 0; index < maxNumberOfMonsters; index++)
                {
                    var monsterUI = Instantiate(this.graphicComponents.monster,this.graphicComponents.field1.transform,false);
                    monsterUI.SetActive(false);
                    this.monsterPoolUI.Add(monsterUI);
                }

            };


            //Initial Phase
            this.player1.DrawInitialHand(InitialNumberOfCards, HandSize);
            this.player2.DrawInitialHand(InitialNumberOfCards, HandSize);
            this.CreateHandUI(this.player1.hand,this.graphicComponents.player1Hand);
            CreateMonsterPool(MaxNumberOfMonsters);
            

            //Someone has to go first
            // int who = Random.Range(0,2); //math domain: [0,2)
            

            //Create turns
            
            this.graphicComponents.currentTurnText.text = player1.name;
            //enter stream
            var stream = Observable.EveryUpdate().Where(deltaTime => Input.GetKeyDown(KeyCode.Space));
            //
            var clickStream = Observable.EveryUpdate().Where(_=>Input.GetMouseButtonDown(0));
            clickStream.Subscribe(_=>
            {
                var mousePosition = Mouse.ScreenToWorldPosition();
                var hit = Physics2D.Raycast(mousePosition,Vector2.zero);
                if(hit)
                {
                    ClickRouter(hit.collider.gameObject);
                }
                else //clicked nothing
                {
                    if(this.store.state == State.ChoosingEnemy)
                    {
                        BackToIdleState();
                    }
                }
            });
            
            this.graphicComponents.endTurnButton.onClick.AddListener(EndTurn);


            
            

            
            // this.playerAI.Play();
            

        }

        void ClickRouter(GameObject clickedObject)
        {
            
            if(clickedObject.HasComponent<UI.Monster>())
            {
                var monster = clickedObject.GetComponent<UI.Monster>();
                MonsterOnClick(monster);
            }
            else if(clickedObject.HasComponent<UI.Card>())
            {
                var card = clickedObject.GetComponent<UI.Card>();
                CardOnClick(card);
            }
        }

        void EndTurn()
        {
            this.store.PrepareNextTurn();

            
            //UI Refresh
            this.graphicComponents.currentTurnText.text = $"Current Turn: {this.store.NextPlayer.gameName}";
            RefreshHandData();
            //AI
            if (this.store.CurrentPlayer == player2)
            {
                //Already drew card
                this.playerAI.Play(); //Let the AI do their whole turn
                this.EndTurn();
                {
                    GetActiveMonsters().Where(monster=>monster.GetComponent<UI.Monster>().IsDead).Select(monster=>monster).ToList().ForEach(monster=>monster.gameObject.SetActive(false));
                }
                
            }
        }

        Models.Monster PlayMonsterCard(Player player, CardData selectedCard)
        {
            
                player.PlayCard(selectedCard);
                //For now just minions, and then summon it
                return player.Summon(selectedCard);
                
        }

        void CreateHandUI(Hand hand, GameObject handUI)
        {
            //Create  Card objects for pooling
            
            Action CreateUICardPool = ()=>
            {
                for (int i = 0; i < HandSize; i++)
                {
                    
                    var card = Instantiate(this.graphicComponents.card, handUI.transform, false).GetComponent<Card>();
                    this.handPoolUI.Add(card.gameObject);
                    card.gameObject.SetActive(false);
                }
            };

            CreateUICardPool();
            RefreshHandData();
            
        }

        void RefreshHandData()
        {
            for (int index = 0; index < player1.hand.cards.Count; index++)
            {

                //There's a 1:1 relation between the UI card and the logic card
                var logicCard = player1.hand.cards[index];
                var uiCard = handPoolUI[index].GetComponent<Card>();
                uiCard.SetData(logicCard);
                uiCard.gameObject.SetActive(true);
                //On Click was here
            }
        }

       

        List<GameObject> GetInactiveMonsters()
        {
            return this.monsterPoolUI.Where(monster => !monster.activeSelf).ToList();
        }

        List<GameObject> GetActiveMonsters()
        {
            return this.monsterPoolUI.Where(monster => monster.activeSelf).ToList();
        }

        void SummonInField(Player player,Models.Monster logicMonster)
        {
            //check whose side you summoning
            var summoningField = (player==this.player1)?this.graphicComponents.field1.transform:this.graphicComponents.field2.transform;
            var monster = GetInactiveMonsters().FirstOrDefault().GetComponent<UI.Monster>();
            monster.transform.SetParent(summoningField);
            monster.gameObject.SetActive(true);
            monster.SetData(logicMonster);
            monster.owner = player;
            //Add click functionality?
            
            
        }

        void CardOnClick(UI.Card card)
        {
            //Also check, IF ITS PLAYER'S 1 TURN
            if (player1.CheckEnergyCost(card.data) && this.store.CurrentPlayer == player1)
            {

                var monster = PlayMonsterCard(player1, card.data);
                SummonInField(player1, monster);
                //Remove from list and add to the end of the pool
                card.gameObject.SetActive(false);
                //Add it to the end of the pool
                handPoolUI.Remove(card.gameObject);
                handPoolUI.Add(card.gameObject);

            }
        }

        void BackToIdleState()
        {
            this.store.state = State.Idle; //back to normal
            this.source.EnableMouseHovering();
            this.source.outlineSelection.enabled = false;
        }
        void MonsterOnClick(UI.Monster monster)
        {
            if (this.store.state == State.ChoosingEnemy)
            {
                if (monster.owner != source.owner && this.store.CanCurrentPlayerAttack)
                { //means they are enemies
                  //attack!!
                    Debug.Log($"{source.logic.Name} attacks {monster.logic.Name}");
                    source.Attack(monster);
                    this.store.DisablePlayerAttack();
                    //Now that they attacked each other, did someone died?
                    if (monster.IsDead) DestroyMonster(monster);
                    if (source.IsDead) DestroyMonster(source);
                    RefreshHandData();
                    this.source = null;
                }
                BackToIdleState();                

            }
            else //Choosing your monster
            {
                monster.ChangeOutlineToAttack();
                monster.DisableMouseHovering();
                this.store.state = State.ChoosingEnemy;
                this.source = monster;
                
            }

        }

        void DestroyMonster(UI.Monster monster)
        {
            monster.gameObject.SetActive(false);
        }

        internal class TurnOrder
        {
            private Queue<Player> order = new Queue<Player>();

            public bool IfItsYourTurn(Player player)
            {
                return player!=this.order.Peek();
            }

            public TurnOrder(Player one, Player two)
            {
                order.Enqueue(one);
                order.Enqueue(two);
            }

            public Player NextPlayer
            {
                get
                {
                    var nextPlayer = order.Dequeue();
                    order.Enqueue(nextPlayer);
                    return nextPlayer;
                }
            }

            public Player CurrentPlayer
            {
                //The last in queue is the current player
                get{return this.order.ToArray()[1];}
            }

            public Player Peek()
            {
                return order.Peek();
            }
        }

        [System.Serializable]
        public struct GraphicComponents
        {
            public GameObject card;
            public GameObject monster;
            public Button endTurnButton;
            [Header("Player 1")]
            public GameObject player1Hand;
            public GameObject field1;
            public Text playerEnergy;
            public Text deckLeft;

            [Header("Player 2")]
            public GameObject field2;
            public Text player2NumberOfCards;

            
            public Text currentTurnText;
        }

        
    }

}