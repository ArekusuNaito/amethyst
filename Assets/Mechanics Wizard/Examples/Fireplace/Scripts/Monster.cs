﻿namespace Fireplace.Models
{
    using System.Collections;
    using System.Collections.Generic;
    using UniRx;
    using Virtues.Services;

    public class Monster
    {
        private Player owner;
        CardData data;
        public IntReactiveProperty attack = new IntReactiveProperty(0);
        public IntReactiveProperty health = new IntReactiveProperty(0);

        bool canAttack=false;

        public Monster(CardData data,Player owner)
        {
            this.data = data;
            this.attack.Value = (int)data.attack;
            this.health.Value = (int)data.health;
            this.owner = owner;
            
        }
        public void Attack(Monster enemy)
        {
            //Here comes a cross attack
            BattleService.Attack(this.attack.Value,enemy.health);
            BattleService.Attack(enemy.attack.Value,this.health);
            //After the battle we have to see if someone died.
            //While this is probably not the best place to put there should be
            // a function that both attacks and destroys the minions that attacked
            if(this.IsDead)this.owner.field.Destroy(this);
            if(enemy.IsDead)enemy.owner.field.Destroy(enemy);
            //
            if(!this.IsDead && enemy.IsDead)
            {
                // UnityEngine.Debug.Log($"{this.owner.name} draws a card");
                this.owner.DrawCard();
            }
            
            DisableAttack();
        }

        

        public void EnableAttack()
        {
            this.canAttack=true;
        }
        public void DisableAttack()
        {
            this.canAttack = false;
        }

        public string Name
        {
            get{return this.data.name;}
        }

        public bool IsDead
        {
            get{return Virtues.Services.BattleService.IsItDead(this.health.Value); }
        }

        public bool CanAttack
        {
            get{return canAttack;}
        }
    }

}