﻿namespace Fireplace
{
    using Virtues;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Models;
    using UniRx;

    public class Player : MonoBehaviour
    {
        public string gameName;
        public IntReactiveProperty currentEnergy = new IntReactiveProperty(3);
        public IntReactiveProperty maxEnergy = new IntReactiveProperty(3);
        
        public Hand hand;
        public Deck deck;

        public Battlefield field = new Battlefield();

        void Awake()
        {
            //Remember, we have to clone the original deck, otherwise the file will be manipulated during play-time
            this.deck = Kernel.CloneScriptableObject<Deck>(this.deck);
        }

        public void DrawCard()
        {
            var nextCard = this.deck.DrawNextCard();
            if(nextCard)
            {
                if(!hand.IsFull)
                {
                    this.hand.Add(nextCard);
                }
                // else //My Hand is full!
                
            }//else: I'm out of cards!
        }

        void CreateEmptyHand(int maxSize)
        {
            this.hand = ScriptableObject.CreateInstance<Hand>();
            this.hand.SetMaxSize(maxSize);
        }
        /// <summary>
        /// //Draw from deck, and then remove them from deck
        /// </summary>
        /// <param name="handSize"></param>
        /// 
        public void DrawInitialHand(int initialNumberOfCards,int handSize)
        {
            CreateEmptyHand(handSize);
            for (int index = 0; index < initialNumberOfCards; index++)
            {
                this.DrawCard();
            }
            // this.hand.cards = this.deck.cards.GetRange(0, handSize);
            // this.deck.cards.RemoveRange(0,handSize);
            
        }

        public void RestoreEnergy()
        {
            this.currentEnergy.Value = this.maxEnergy.Value; //restore energy
        }

        public bool CheckEnergyCost(CardData card)
        {
            if (this.currentEnergy.Value >= card.cost) return true;
            else return false;
        }

        public void PlayCard(CardData card)
        {
            //Does this card has a "Play" effect?
            //card.activate(PlayEffect);
            this.currentEnergy.Value-=card.cost;
            this.hand.Play(card);
            
        }

        public Models.Monster Summon(CardData cardData)
        {
            return this.field.Summon(cardData,this);
        }

        public void EnableMonstersAttack()
        {
            this.field.EnableAttackForAllMonsters();
        }
        public void DisableMonstersAttack()
        {
            this.field.DisableAttackForAllMonsters();
        }

    }

}