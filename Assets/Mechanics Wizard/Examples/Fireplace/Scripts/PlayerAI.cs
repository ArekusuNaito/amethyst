﻿namespace Fireplace.AI
{
    using Models;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using System;
    using System.Linq;

    public class PlayerAI 
    {
        private Player me,enemy;
        private GameMaster.Store store;

        public Action<Player,Monster> SummonUI;
        public PlayerAI(ref Player player, ref Player enemy, ref GameMaster.Store store)
        {
            this.me = player;
            this.enemy = enemy;
            this.store = store;
        }

        public void Play()
        {
            if(DoIHaveEnergy)
            {
                SummonThings();
                if (DoIHaveMonsters && store.CanCurrentPlayerAttack)
                {
                    ExecuteAttackBehaviour();
                }
            }
            else
            {
                if(DoIHaveMonsters && store.CanCurrentPlayerAttack)
                {
                    ExecuteAttackBehaviour();
                }
            }
        }

        void ExecuteAttackBehaviour()
        {
            
            var myBestMinion = me.field.BestAttackMonster;
            var weakMinion = FindWeakMinion(myBestMinion);
            Action<Monster> MinionsAttack = (enemyMinion)=>
            {
                Debug.Log($"AI uses {myBestMinion.Name} to attack {enemyMinion.Name}");
                myBestMinion.Attack(enemyMinion);
                this.store.DisablePlayerAttack();
            };
            //
            if (weakMinion == null) //No weak minions,
            {
                var strongEnemyMinion = FindValueTrade(myBestMinion);
                if(strongEnemyMinion!=null)
                {
                    MinionsAttack(strongEnemyMinion);
                }
                
            }
            else
            {
                MinionsAttack(weakMinion);
            }
        }

        void SummonThings()
        {
            var myHand = this.me.hand.cards;
            var strongestEnemyMonster = this.enemy.field.BestAttackMonster;
            var bigMonsterToPlay = (from myCard in me.hand.cards
            where me.currentEnergy.Value>=myCard.cost
            orderby myCard.cost descending 
            select myCard);
            bigMonsterToPlay.ToList().ForEach(card=>
            {
                if(me.CheckEnergyCost(card))
                {
                    me.PlayCard(card);
                    var monster = me.Summon(card);
                    this.SummonUI(me,monster);
                    Debug.Log($"{me.name} summons {monster.Name}");
                    //here there should be a connection to UI?
                }
            });
            
            
        }

        /// <summary>
        /// Given one of my minions, I will find a minion who is weaker than mine and that I can defeat.
        /// </summary>
        /// <param name="myMonster"></param>
        /// <returns></returns>
        Monster FindWeakMinion(Monster myMonster)
        {
            var enemyMonsters = this.enemy.field.monsters;
            var target = (from enemyMonster in enemyMonsters
            where enemyMonster.health.Value<=myMonster.attack.Value && enemyMonster.attack.Value<myMonster.health.Value
            orderby enemyMonster.health.Value descending
            select enemyMonster).FirstOrDefault();
            // Debug.Log($"I {myMonster.Name} should attack {target.Name}");
            return target;
        }

        /// <summary>
        /// Look for a minion stronger than mine, but that my weak minion can kill
        /// </summary>
        /// <param name="myMonster"></param>
        /// <returns></returns>
        Monster FindValueTrade(Monster myMonster)
        {
            var enemyMonsters = this.enemy.field.monsters;
            var target = (from enemyMonster in enemyMonsters
            where enemyMonster.attack.Value>=myMonster.attack.Value && enemyMonster.health.Value<=myMonster.attack.Value
            orderby enemyMonster.attack.Value descending
            select enemyMonster).FirstOrDefault();
            return target;
        }

        bool DoIHaveEnergy
        {
            get
            {
                return me.currentEnergy.Value>1;    
            }
        }
        bool DoIHaveMonsters
        {
            get
            {
                return me.field.HasMonsters;;
            }
        }
    }

}