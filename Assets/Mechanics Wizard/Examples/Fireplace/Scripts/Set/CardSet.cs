﻿namespace Fireplace.Models
{
    using Models;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UniRx;


    [System.Serializable]
    public class CardSet : ScriptableObject
    {

        public List<CardData> cards = new List<CardData>();

        public void Add(CardData card)
        {
            this.cards.Add(card);
        }
        
        public CardData NextCard
        {
            get
            {
                if (cards.Count > 0) return this.cards[0];
                else return null;
            }
        }

        public void Shuffle()
        {
            
        }
    }

}