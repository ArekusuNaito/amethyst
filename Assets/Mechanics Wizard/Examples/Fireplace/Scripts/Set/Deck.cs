﻿namespace Fireplace.Models
{
    using UnityEngine;
    [CreateAssetMenu(fileName = "New Deck", menuName = "Fireplace/New Deck", order = 1)]
    public class Deck : CardSet
    {
        public CardData DrawNextCard()
        {
            if(this.cards.Count>0)
            {
                var nextCard = this.cards[0];
                this.cards.RemoveAt(0);
                return nextCard;
            }
            else return null;
        }
    }
}