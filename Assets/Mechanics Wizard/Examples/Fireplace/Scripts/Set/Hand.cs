﻿namespace Fireplace.Models
{
    
    public class Hand : CardSet
    {
        private int maxSize=1;
        public int MaxSize{get{return maxSize;}}
        public void Play(CardData card)
        {
            
            //After playing it, its no longer in your hand
            this.cards.Remove(card);
        }
        
        public void SetMaxSize(int maxSize)
        {
            this.maxSize=maxSize;
        }

        public bool IsFull {get { return this.cards.Count == maxSize; } }
        
    }
}