﻿namespace Fireplace.UI
{
    using DG.Tweening;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Models;
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public class BaseCardUI:MonoBehaviour
    {
        public Outline outlineSelection;
        public Color mouseHoverColor;
        
        public Text attack;
        public Text health;

        protected virtual void Awake()
        {
            this.outlineSelection.effectColor = mouseHoverColor;
            
        }

        void AddHoverListeners()
        {
            this.OnMouseEnterAsObservable().Subscribe(_ =>
            {
                {
                    this.outlineSelection.enabled = true;
                }

            }).AddTo(this);

            this.OnMouseExitAsObservable().Subscribe(_ =>
            {
                {
                    this.outlineSelection.enabled = false;
                }
            }).AddTo(this);
        }

        

        
    }
    public class Card : BaseCardUI
    {
        public CardData data;
        
        [Header("UI Components")]
        public Image backgroundImage;
        public Image monsterImage;
        public Text cardName;

        private Tween monsterRotateAnimation;
        

        public void SetData(CardData data)
        {
            this.data = data;
            this.attack.text = this.data.attack.ToString();
            this.health.text = this.data.health.ToString();
        }

        override protected void Awake()
        {
            base.Awake();
            // this.monsterRotateAnimation = 
            
            monsterRotateAnimation = monsterImage.gameObject.transform.DOScaleX(0,1).From(1).SetEase(Ease.InOutCubic).SetLoops(-1,LoopType.Yoyo);
            monsterRotateAnimation.Pause();
            this.OnMouseEnterAsObservable().Subscribe(_ =>
            {
                {
                    monsterRotateAnimation.Play();
                    this.outlineSelection.enabled = true;
                }

            }).AddTo(this);

            this.OnMouseExitAsObservable().Subscribe(_ =>
            {
                {
                    monsterRotateAnimation.Restart();
                    monsterRotateAnimation.Pause();
                    this.outlineSelection.enabled = false;
                }
            }).AddTo(this);
            
        }


    }

}