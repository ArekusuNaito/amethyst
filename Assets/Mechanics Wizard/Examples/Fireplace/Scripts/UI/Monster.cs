﻿namespace Fireplace.UI
{
    using DG.Tweening;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Models;
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine.Events;
    using UnityEngine.UI;
    

    public class Monster : BaseCardUI
    {

        [Header("Animations")]
        [Range(0,20)]
        public float idleAnimationMovement=4;

        public Player owner;
        public Models.Monster logic;
        private RectTransform rectTransform;
        private Tween idleAnimation;
        private Vector3 startingPosition;
        public Color attackSelectColor;
        private bool canHover = true;
        override protected void Awake()
        {
            base.Awake();
            
            this.rectTransform = GetComponent<RectTransform>();

            idleAnimation = CreateIdleAnimation();
            //---
            this.OnMouseEnterAsObservable().Subscribe(_ =>
            {
                if(canHover)
                {
                    this.outlineSelection.enabled = true;
                }
                
            }).AddTo(this);
            
            this.OnMouseExitAsObservable().Subscribe(_ =>
            {
                if(canHover)
                {
                    this.outlineSelection.enabled = false;
                }
            }).AddTo(this);
            
            


        }

        
        
        public void EnableMouseHovering()
        {
            this.outlineSelection.effectColor = mouseHoverColor;
            this.canHover=true;
        }

        public void DisableMouseHovering()
        {
            this.canHover=false;
        }

        public void ChangeOutlineToAttack()
        {
            this.outlineSelection.effectColor = attackSelectColor;
        }

        void Update()
        {
            
        }

        Tween CreateIdleAnimation()
        {
            return rectTransform.DOLocalMoveY(startingPosition.y+idleAnimationMovement,3.2f).From(startingPosition.y-idleAnimationMovement).SetEase(Ease.OutCubic).SetLoops(-1,LoopType.Yoyo);
        }

        public void SetData(Models.Monster data)
        {

            this.startingPosition = rectTransform.localPosition;
            this.logic = data;
            this.logic.attack.SubscribeToText(this.attack,newValue=> newValue.ToString());
            this.logic.health.SubscribeToText(this.health,newValue=> newValue.ToString());
        }

        public void Attack(Monster target)
        {
            //Time for a cross attack!
            this.logic.Attack(target.logic);
            
        }

        public bool IsDead
        {
            get{return this.logic.IsDead;}
        }
        
        public void RemoveFromField()
        {
            this.owner.field.Destroy(this.logic);
        }

        public bool CanAttack
        {
            get{return this.logic.CanAttack;}
        }



    }

}