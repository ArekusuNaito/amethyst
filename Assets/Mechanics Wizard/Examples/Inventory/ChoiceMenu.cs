﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ChoiceMenu : MonoBehaviour
{
    public Selectable selectable;
    void Start()
    {
        var foo = selectable.GetComponent<EventTrigger>();
        foo.triggers.Add(CreateOnSubmit(data=>Debug.Log("Aver")));
        // var bar = EventTriggerType.Select;

    }
    
    EventTrigger.Entry CreateOnSelect(UnityAction<BaseEventData> callBack)
    {  
        var onSelect = new EventTrigger.Entry();
        onSelect.eventID = EventTriggerType.Select;
        onSelect.callback.AddListener(callBack);
        return onSelect;
    }

    EventTrigger.Entry CreateOnSubmit(UnityAction<BaseEventData> callBack)
    {  
        var onSubmit = new EventTrigger.Entry();
        onSubmit.eventID = EventTriggerType.Submit;
        onSubmit.callback.AddListener(callBack);
        return onSubmit;
    }
    
    
}
