﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UniRx;




public class DialogueWindow : MonoBehaviour
{
    // Start is called before the first frame update
    Text textArea;
    public UnityEvent onWrite;
    public UnityEvent onConversationEnd;
    public UnityEvent onNextDialogue;
    public UnityEvent onDialogueEnded;
    public DialogueWindowOptions options;
    Conversation conversation;
    [SerializeField]protected float writerLetterPeriodMilliseconds;
    [Header("Input")]
    public InputActionMap input;
    enum State{Writing,WaitingInput};
    CompositeDisposable disposables = new CompositeDisposable();

    void Start()
    {
        
        
        this.textArea = GetComponentInChildren<Text>();
        input["Submit"].performed+= context=>
        {
            // Debug.Log("SPACE");
            if(context.performed)
            {
                disposables.Clear();
                if(!conversation.Ended)
                {
                    this.onNextDialogue.Invoke();
                    CreateNext();
                }
                else
                {
                    this.input.Disable();
                    this.onConversationEnd.Invoke();
                    this.conversation.onEnd.Invoke();
                    this.gameObject.SetActive(false);
                }
                
            }
        };
    }

    // Update is called once per frame
    public void Open(Conversation conversation)
    {
        // this.conversation = new Conversation(dialogues);
        Debug.Log("Start Conversation");
        this.conversation = conversation;
        this.textArea.text="";
        this.conversation.Reset();
        this.gameObject.SetActive(true);
        this.disposables.Clear();
        CreateNext();
    }
    void CreateNext()
    {
        input.Disable();
        var nextDialogue = this.conversation.GetNextDialogue();
        if(nextDialogue!=null)
        {
            //NOTE: Consider the fact that at Low Framerates, this might not work properly.
            //Let's dive into this issue after we program other features, to keep the work going
            //Just keep it in mind. Using the "while" on the update is a way to fix this.
            //Could also think about in "Frame" terms. Draw a letter every "X" amount of frames.
            Observable.Timer(TimeSpan.FromTicks(0),TimeSpan.FromMilliseconds(writerLetterPeriodMilliseconds)).Take(nextDialogue.Length+1)
            .DoOnCompleted(()=>
            {
                input.Enable();
                this.onDialogueEnded.Invoke();
            })
            .Subscribe(index=>
            {
                textArea.text = nextDialogue.Substring(0,(int)index);
                this.onWrite.Invoke();
            }).AddTo(disposables);
        }
        else Debug.Log("Next conversation is null");
        
    }

}
