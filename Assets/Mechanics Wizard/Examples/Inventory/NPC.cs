﻿using UnityEngine;
using UnityEngine.Events;
namespace MechanicsWizard.InventoryExample
{
    using System.Collections;
    using System.Collections.Generic;
    using Virtues;
    using Virtues.Systems;

    public class NPC : MonoBehaviour,Interactable
    {
        // Update is called once per frame
        public Conversation conversation;
        public UnityEvent onInteract;
        public UnityEvent onConversationEnd;

        protected virtual void Awake()
        {
            if(conversation!=null)
            {
                conversation.onEnd.AddListener(()=>
                {
                    this.onConversationEnd.Invoke();
                });
            }
            
        }
        void OnCollisionEnter2D(Collision2D collision)
        {
            this.Interact();
        }

        public void Interact()
        {
            this.onInteract.Invoke();
        }
    }

}