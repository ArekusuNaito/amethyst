﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Virtues;
using Virtues.Systems;

public class PlayerData : MonoBehaviour
{
    public Inventory inventory;
    public Float currency;
    // public  scene;
    public SceneObject scene;
}
