﻿namespace MechanicsWizard.InventoryExample
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Virtues;
    using Virtues.Systems;
    using Virtues.Models;
    [CreateAssetMenu(fileName = "New Shop", menuName = "Virtues/Systems/Shop")]
    public class Shop : ScriptableObject
    {
        public Inventory inventory;
        [SerializeField]Inventory targetInventory;
        [SerializeField]Float targetCurrency;

        public Inventory TargetInventory{set{this.targetInventory=value;}}
        public Float TargetCurrency{set{this.targetCurrency=value;}}
        bool CanItBeAfforded(float currency,Priceable priceable,int quantityToBuy=1)
        {
            return currency>=GetTotalPrice(priceable,quantityToBuy);
        }
        
        float GetTotalPrice(Priceable priceable,int quantity)
        {
            return priceable.Price.Value*quantity;
        }
        public void SetTargetInventory(Inventory target)
        {
            this.targetInventory=target;
        }

        public void SetTargetCurrency(Float target)
        {
            this.targetCurrency=target;
        }
        public void Buy(Item item,int quantity)
        {
            var totalPrice = GetTotalPrice(item,quantity);
            if(CanItBeAfforded(this.targetCurrency,item,quantity))
            {
                Debug.Log($"Buying {item.name}; Before: {this.targetCurrency.Value}");
                this.targetCurrency.Value-=totalPrice;
                targetInventory.Add(Kernel.CloneScriptableObject<Item>(item));
                Debug.Log($"After: {this.targetCurrency.Value}");
            }
            
            
        }
        
    }

}