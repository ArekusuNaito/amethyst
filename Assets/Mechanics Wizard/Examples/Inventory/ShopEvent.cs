﻿namespace MechanicsWizard.InventoryExample
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using Virtues.Models;

    [System.Serializable]
    public class ShopEvent : UnityEvent<Item,int>
    {
        
    }

}