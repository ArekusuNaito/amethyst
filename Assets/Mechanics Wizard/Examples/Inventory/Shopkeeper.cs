﻿namespace MechanicsWizard.InventoryExample
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Virtues;

    public class Shopkeeper : NPC
    {
        public Shop shop;
        PlayerData playerData;
        [SerializeField]ShopUI shopUI;

        override protected void Awake()
        {
            base.Awake();
            shopUI.Shop=this.shop;
            this.onConversationEnd.AddListener(()=>
            {
                this.shopUI.gameObject.SetActive(true);
            });
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            if(other.gameObject.TryGetComponent<PlayerData>(out this.playerData))
            {
                Debug.Log("My dude");
                this.shop.TargetInventory=playerData.inventory;
                this.shop.TargetCurrency=playerData.currency;
                this.Interact();
            }
        }
    }

}