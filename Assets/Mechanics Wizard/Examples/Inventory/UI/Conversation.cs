﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "New Conversation", menuName = "Virtues/Systems/Dialogue/Conversation", order = 1)]
public class Conversation : ScriptableObject
{
    [TextArea]
    public List<string> dialogues;
    int index=0;
    public Conversation(List<string> dialogues)
    {
        this.dialogues=dialogues;
    }

    public void Reset()
    {
        this.index=0;
    }
    public string GetNextDialogue()
    {
        if(index>=dialogues.Count)return null;
        return dialogues[index++];
    }
    public string CurrentDialogue{get{return this.dialogues[index];}}
    public bool Ended{get{return index==dialogues.Count;}}
    public UnityEvent onEnd;
}
