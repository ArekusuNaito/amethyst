﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DialogueWindowOptions", menuName = "Virtues/UI/DialogueWindowOptions", order = 1)]
public class DialogueWindowOptions:ScriptableObject
{
    public SFX soundEffects;
    [System.Serializable]
    public struct SFX
    {
        public AudioClip onLetterWrite;
        public AudioClip onDialogueEnd;
        public AudioClip onNextDialogue;
        public AudioClip onLastDialogueCursor;
        public AudioClip onConversationEnd;
    }
}
