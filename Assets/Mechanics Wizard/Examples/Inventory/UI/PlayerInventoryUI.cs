﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
namespace MechanicsWizard.InventoryExample
{
    using System.Collections;
    using System.Collections.Generic;
    using Virtues.Systems;
    using Virtues;

    public class PlayerInventoryUI : MonoBehaviour
    {
        public GameObject selectablePrefab;
        public Inventory inventory;


        void OnEnable()
        {
            
            var items = inventory.items.Select(item=>
            {
                var itemUI = Instantiate(selectablePrefab,this.transform).GetComponent<SelectableUI>();
                // shopItemUI.Set(item,()=>buy.Invoke(item,selectedQuantity));
                itemUI.Set(item);
                return itemUI;
            }).ToList();
            
        }

        void OnDisable()
        {
            this.transform.DestroyChildren();
        }
    }

}