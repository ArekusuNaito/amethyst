﻿using UnityEngine;
using UnityEngine.UI;
namespace MechanicsWizard.InventoryExample
{
    using System.Collections;
    using System.Collections.Generic;
    using Virtues.Models;
    public class SelectableUI : MonoBehaviour
    {
        public Item data;
        public Text label;
        public UnityEngine.Events.UnityEvent onSelect;

        public void Set(Item item)
        {
            this.data = item;
            this.label.text = this.data.name;
        }
    }

}