﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
namespace MechanicsWizard.InventoryExample
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Virtues.Models;

    public class ShopItemUI : MonoBehaviour
    {
        public Item data;
        public Text label;
        public Button button;

        public void Set(Item item,Action action)
        {
            this.data = item;
            this.label.text = this.data.name;
            this.button.onClick.AddListener(()=>action());
        }
    }
    

}