﻿using UnityEngine;
using System.Linq;
namespace MechanicsWizard.InventoryExample
{
    using System.Collections;
    using System.Collections.Generic;
    using Virtues;
    using Virtues.Systems;

    public class ShopUI : MonoBehaviour
    {
        Shop shop;
        public GameObject itemPrefab;
        public int selectedQuantity=1;

        public ShopEvent buy;


        public Shop Shop{set{this.shop=value;}}

        void Start()
        {
            Debug.Log(this.shop.name);
            Debug.Assert(this.buy!=null,"evento es nulo");
            Debug.Assert(this.shop!=null,"tienda es nulo");
            this.buy.AddListener(this.shop.Buy);
        }
        void OnEnable()
        {
            Debug.Log("Shop is open");
            var items = shop.inventory.items.Select(item=>
            {
                var shopItemUI = Instantiate(itemPrefab,this.transform).GetComponent<ShopItemUI>();
                shopItemUI.Set(item,()=>buy.Invoke(item,selectedQuantity));
                return shopItemUI;
            }).ToList();
            
        }
    }

}