﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Virtues;
public class UnMenMoviendose : MonoBehaviour
{
    Rigidbody2D body;
    Animator animator;
    SpriteRenderer spriteRenderer;
    void Awake()
    {
        this.body = GetComponent<Rigidbody2D>();
        this.animator = GetComponent<Animator>();
        this.animator.SetFloat("Facing",2);
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        
    }   

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            this.body.SetVelocityX(4);
            this.animator.speed=1;
            
            this.spriteRenderer.flipX=true;
        }
        else if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            this.body.SetVelocityX(-4);
            this.animator.speed=1;
            this.spriteRenderer.flipX=false;

        }
        else if(Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            this.body.velocity = Vector2.zero;
            this.animator.speed=0;
        }
    }
}
