﻿using UnityEngine;
namespace MechanicsWizard.SimplePlatformer
{
    using System.Collections;
    using System.Collections.Generic;
    using UniRx;
    using UniRx.Triggers;
    using Virtues;
    using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class CoinCollect : MonoBehaviour
{
    // Start is called before the first frame update
    // public Event onCollect;
    public ParticleSystem particles;
    public UnityEvent onCollect;
    


    void Start()
    {
        this.OnTriggerEnter2DAsObservable().Subscribe(collider=>
        {
            if(collider.transform.HasComponent<Player>())
            {
                this.onCollect.Invoke();
                var particleClone = Instantiate(this.particles,this.transform.position,Quaternion.identity);
                Destroy(particleClone,particleClone.main.duration);
                Destroy(this.gameObject);
                
            }
        }).AddTo(this);
    }

    
    
}

}