﻿namespace MechanicsWizard.SimplePlatformer
{
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UniRx;
using Virtues;
public class Player : MonoBehaviour
{
    [SerializeField]public Rigidbody2D body;
    public BoxCollider2D boxCollider;
    public Animator animator;
    SpriteRenderer spriteRenderer;
    MechanicsInputMap input;
    // Start is called before the first frame update
    
    Vector2 leftStick = new Vector2(0,0);
    [Header("Horizontal Movement")]
    public float xSpeed = 1;
    [Header("Vertical Movement")]
    public float timeToApex = 0.5f;
    public float smallJumpUnits = 2.5f;
    public float completeJumpUnits = 5f;
    private float jumpVelocity;
    [Header("Collisions")]
    public float castDistance = 0.016f;
    public LayerMask groundMask;
    void OnEnable()
    {
        this.input.Enable();
    }
    void Awake()
    {
        this.input = new MechanicsInputMap();
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        //
        this.body.gravityScale = NewGravity(this.smallJumpUnits,this.timeToApex)/Mathf.Abs(Physics2D.gravity.y);
        this.jumpVelocity = Mathf.Abs(Physics2D.gravity.y)*timeToApex*this.body.gravityScale;
    }

    float NewGravity(float distance,float time)
    {
        var newGravity = 2*distance/(Mathf.Pow(time,2));
        // Debug.Log(newGravity);
        return newGravity;
    }
    

    void Update()
    {
        //GetInput
        this.leftStick = this.input.SimplePlatformer.HorizontalAxis.ReadValue<Vector2>();
        
        
        var xSpeedWithInput = leftStick.x*this.xSpeed;
        //
        if(xSpeedWithInput<0)this.spriteRenderer.flipX=true;
        if(xSpeedWithInput>0)this.spriteRenderer.flipX=false;
        this.animator.SetFloat("xSpeed",Math.Abs(xSpeedWithInput));
        this.animator.SetFloat("ySpeed",this.body.velocity.y);
        this.animator.SetBool("onGround",this.onGround);
        //
        this.body.SetVelocityX(xSpeedWithInput);
        if(this.input.SimplePlatformer.Jump.triggered && this.onGround)
        {
            this.body.AddForce(jumpVelocity*Vector2.up,ForceMode2D.Impulse);
            this.onGround=false;
            Debug.Log("Jump");
        }
        
        
    }

    public bool onGround=false;
    void FixedMove(float xInput,bool jumpInput)
    {
        this.body.SetVelocityX(xInput*xSpeed);
    }

    

    void GroundCheck()
    {
        var boxSize = new Vector2(this.boxCollider.size.x-castDistance*2,castDistance);
        this.onGround = Physics2D.OverlapBox(this.boxCollider.bounds.CenterDown(),boxSize,0,this.groundMask);   
        
        
    }
    void FixedUpdate()
    {   
        this.GroundCheck();
    }

}


}