﻿namespace MechanicsWizard.SimplePlatformer
{
    
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;
    using Virtues;
    using UniRx;
    public class WinCondition : MonoBehaviour
    {
        public Text collectionUI;
        public GameObject collectables;
        int collectableCount;
        public Float currentCollectables;
        public UnityEvent onWin;
        

        void Awake()
        {
            this.collectableCount = this.collectables.transform.childCount;
            // Debug.Log(collectableCount);
            this.currentCollectables.SubscribeToText(this.collectionUI,newValue=>
            {
                var percentage = ((newValue/collectableCount)*100).ToString("#.##");
                return $"COLLECTED: {percentage}%";

            });
            //When Everything has been collected
            this.currentCollectables.Observable.Where(newValue=>newValue>=this.collectableCount).Subscribe(totalCount=>
            {
                Debug.Log(totalCount);
                onWin.Invoke();
            });
        }
        
    }

}