﻿namespace MechanicsWizard.SimpleTopDown
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using Virtues;
    using Virtues.Audio;
    //NOTE: While this is quick and useful. Maybe on the long-term will have to invest on a Pool of Destroy animations
    public class DestroyAnimation: MonoBehaviour
    {
        [SerializeField]protected GameObject destroyAnimationPrefab;
        [SerializeField]protected float animationSpeed=1;
        public void Play()
        {
            if(destroyAnimationPrefab.HasComponent<Animator>())
            {   
                var effect  = Instantiate(destroyAnimationPrefab,this.gameObject.transform.position,Quaternion.identity);
                var animator = effect.GetComponent<Animator>();
                // animator.GetCurrentAnimatorStateInfo(0).length
                animator.speed=animationSpeed;
                Destroy(effect,animator.GetCurrentAnimatorStateInfo(0).length*animationSpeed);
            }
            else
            {
                Debug.LogError($"Prefab doesn't have an animator");
            }

            
        }
    }

}