﻿using System;
namespace MechanicsWizard.SimpleTopDown
{
    using Virtues;
    using System.Collections;
    using System.Linq;
    using System.Collections.Generic;
    using UnityEngine;
    using UniRx;

    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] GameObject targetPrefab;
        
        [SerializeField] GameObject locationHolder;
        [SerializeField]GameObject normalEnemyHolder;
        [SerializeField] GameObject shooterEnemyHolder;
        //
        EntityPool<PatrolAI> normalEnemies = new EntityPool<PatrolAI>();
        List<Transform> spawnLocations = new List<Transform>();
        List<Renderer> locationRenderers = new List<Renderer>();
        EntityPool<PatrolAI> shooterPool = new EntityPool<PatrolAI>();
        int currentWaveSize=0;
        int maxWaveSize=0;        

        
        void Awake()
        {
            enemyWeight.Add(EnemyType.Chaser,0.75f);
            enemyWeight.Add(EnemyType.Shooter,0.25f);
            shooterPool.FillWithChildrenOf(shooterEnemyHolder,TellEnemiesWhatToDoOnCollision);
            normalEnemies.FillWithChildrenOf(normalEnemyHolder,TellEnemiesWhatToDoOnCollision);
            locationHolder.GetComponentsInChildren<Renderer>(true,locationRenderers);
            CreateWave(8);
            // Observable.Interval(TimeSpan.FromSeconds(1f)).Take(shooterPool.Count).Subscribe(_=>
            // {
            //     // var random = Random.Range(0,spawnLocations.Count-1);
            //     var selectedSpawner = locationRenderers.Where(spawner=>!spawner.isVisible).FirstOrDefault();
            //     if(selectedSpawner)
            //     {
                    
            //         shooterPool.ActivateNextIn(selectedSpawner.transform.position);

            //     }
            //     // var AI = Instantiate(enemyPrefab,locations[random].transform.position,Quaternion.identity)?.GetComponent<PatrolAI>();
            //     // var nextEnemy = shooterPool.GetNext(spawnLocations[random].position);
            //     //Once instantiated; add: when the enemy attacks, should hurt the player
            //     //This was the work-around when used Instantiate.
            //     // DamageTarget = targetPrefab.GetComponent<Damageable>().Damage;
            //     // KnockBackTarget = targetPrefab.GetComponent<Knockbackable>().Knockback;
            //     // AI.onCollision.RemoveAllListeners();
            //     // AI.onCollision.AddListener(targetCollision=>
            //     // {
            //     //     DamageTarget(1);
            //     //     KnockBackTarget(targetCollision);
            //     // });
                
            // }).AddTo(this);
        }

        Dictionary<EnemyType,float> enemyWeight=new Dictionary<EnemyType, float>();
        enum EnemyType{Chaser,Shooter}
        void CreateWave(int waveSize)
        {
            this.currentWaveSize=waveSize;
            this.maxWaveSize = waveSize;
            var invisibleSpawners = locationRenderers.Where(spawner=>!spawner.isVisible).ToList();
            
            
            for (int index = 0; index < waveSize; index++)
            {
                var selectedPool = (Random.Range(0.0f,1.0f)>enemyWeight[EnemyType.Shooter])?this.normalEnemies:this.shooterPool;
                var spawnLocation = invisibleSpawners[Random.Range(0,invisibleSpawners.Count-1)].transform.position;
                var enemy = selectedPool.ActivateNextIn(spawnLocation);
                if(enemy)
                {
                    enemy.chaseSpeed+=0.1f;
                }
                
                
            }
            
            
            
        }

        void TellEnemiesWhatToDoOnCollision(PatrolAI enemy)
        {
            Life enemyLife;
            if(enemy.gameObject.TryGetComponent<Life>(out enemyLife))
            {
                enemyLife.whenNoLifeLeft.AddListener(()=>IncreaseWavePower());
            }
            enemy.onCollision.AddListener(collision=>
            {
                if(collision==null)return;
                if(collision.gameObject.HasComponent<Damageable>())
                {
                    
                    if(collision.contactCount>0)
                    {
                        Action<float> DamageTarget = collision.gameObject.GetComponent<Damageable>().Damage;
                        var knockbackable = collision.gameObject.GetComponent<Knockbackable>();
                        //
                        DamageTarget(1);
                        //
                        {
                            var enemyLayer = enemy.gameObject.layer;
                            var otherLayer = collision.gameObject.layer;
                            Physics2D.IgnoreLayerCollision(enemyLayer,otherLayer,ignore:true);
                            //Re-enable it after the knockback time
                            Observable.Timer(TimeSpan.FromSeconds(knockbackable.KnockbackDuration)).Subscribe(_=>
                            {
                                Physics2D.IgnoreLayerCollision(enemyLayer,otherLayer,ignore:false);
                            }).AddTo(this);
                        }
                        var knockBackDirection = collision.GetContact(0).normal*-1;
                        knockbackable.Knockback(knockBackDirection);
                    }

                }
                
            });
        }

        public void IncreaseWavePower()
        {
            this.currentWaveSize--;
            if(currentWaveSize<=0)
            {
                CreateWave(this.maxWaveSize+1);
                Debug.Log("We need more minions");
            }
        }
    }

}