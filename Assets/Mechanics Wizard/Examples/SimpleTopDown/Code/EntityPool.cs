﻿namespace MechanicsWizard.SimpleTopDown
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using UnityEngine;

    public class EntityPool<T> where T: MonoBehaviour
    {
        List<T> pool = new List<T>();
        public EntityPool()
        {

        }

        public EntityPool(GameObject parent)
        {
            this.FillWithChildrenOf(parent);
        }
        public void FillWithChildrenOf(GameObject parent)
        {
            // parent.GetComponentsInChildren<T>(true,this.pool);
            foreach(Transform child in parent.transform)pool.Add(child.GetComponent<T>());
        }
        public void FillWithChildrenOf(GameObject parent,Action<T> ForEach)
        {
            // parent.GetComponentsInChildren<T>(true,this.pool);
            foreach(Transform child in parent.transform)
            {
                var component = child.GetComponent<T>();
                pool.ForEach(ForEach);
                pool.Add(component);
            }
        }
        public T GetNext()
        {
            return this.pool.Where(entity=>!entity.gameObject.activeInHierarchy).FirstOrDefault();
        }

        public T ActivateNextIn(Vector3 newPosition,bool activate=true)
        {
            var nextOne = this.pool.Where(entity=>!entity.gameObject.activeInHierarchy).FirstOrDefault();
            if(nextOne)
            {
                nextOne.gameObject.SetActive(activate);
                nextOne.transform.position = newPosition;
            }
            return nextOne;
        }
        public void ForEach(Action<T> action)
        {
            this.pool.ForEach(action);
        }
        public int Count{get{return this.pool.Count;}}
    }

}