﻿namespace MechanicsWizard.SimpleTopDown
{
    using System.Text.RegularExpressions;
    using UnityEngine;
    using UnityEngine.UI;
    using Virtues;

    public class FloatTextListener : MonoBehaviour
    {
        public Float variable;
        public Text text;
        [Tooltip("Use {x} when you want to use the float value")]
        public string regexValue="Eg. Score:{x}";
        string pattern = "({[x|X]})"; //Upper or lower case is fine
        void Awake()
        {
            this.variable.SubscribeToText(text,value=>
            {
                return Parser(regexValue,value);
            });
        }

        string Parser(string input,float value)
        {
            Regex regex = new Regex(pattern);
            var output = regex.Replace(input,value.ToString());
            // Debug.Log(output);
            return output;
        }
    }

}