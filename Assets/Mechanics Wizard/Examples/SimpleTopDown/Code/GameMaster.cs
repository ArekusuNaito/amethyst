﻿namespace MechanicsWizard.SimpleTopDown
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.InputSystem;
    using UnityEngine.Events;
    using UnityEngine.UI;
    using Virtues;

    public class GameMaster : MonoBehaviour
    {
        [SerializeField]GameObject player;
        public UnityEvent onGameOver;
        public UnityEvent onRetry;
        public UnityEvent onBackToMainMenu;
        public UnityEvent onPause;
        public UnityEvent onUnPause;
        void Awake()
        {

        }

        public void GameOver()
        {
            this.onGameOver.Invoke();
        }

        public void Pause(InputAction.CallbackContext context)
        {
            if(context.performed)
            {
                Time.timeScale=0;
                this.onPause.Invoke();
            }
        }

        public void UnPause(InputAction.CallbackContext context)
        {
            if(context.performed)
            {
                Time.timeScale=1;
                this.onUnPause.Invoke();
            }
            
        }

        public void Retry(InputAction.CallbackContext context)
        {
            if(context.performed)
            {
                this.onRetry.Invoke();
            }
        }

        public void BackToMainMenu()
        {
                Time.timeScale=1;
                this.onBackToMainMenu.Invoke();

        }
        
    }

}