﻿namespace MechanicsWizard.SimpleTopDown
{
    using UnityEngine;
    using UniRx;
    using Virtues;
    using UnityEngine.Events;
    using System;
    public class Life: MonoBehaviour,Damageable,Invincible
    {
        [SerializeField]Float LifeObject;
        [SerializeField]float maxLife;
        [SerializeField]bool isInvincible;
        [SerializeField]float invincivilityTime=0.25f;
        public FloatEvent onDamage;
        public UnityEvent whenNoLifeLeft;

        public Float CurrentLife {get{return this.LifeObject;}set{this.LifeObject=value;}}
        public float MaxLife {get{return this.maxLife;}}
        public bool IsInvincible{get{return this.isInvincible;}}
        void Awake()
        {
            this.LifeObject = Kernel.CloneScriptableObject<Float>(LifeObject);
            this.LifeObject.Value=maxLife;
        }
        public void Damage(float damage)
        {
            if(!IsInvincible)
            {
                this.isInvincible=true;
                this.CurrentLife.Value-=damage;
                this.onDamage.Invoke(damage);
                if(this.CurrentLife.Value<=0)
                {
                    whenNoLifeLeft.Invoke();
                } 
                Observable.Timer(TimeSpan.FromSeconds(invincivilityTime)).Subscribe(_=>
                {
                    this.isInvincible=false;
                }).AddTo(this);
                
            }
        }
    }
}