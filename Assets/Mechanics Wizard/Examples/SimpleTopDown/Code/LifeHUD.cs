﻿namespace MechanicsWizard.SimpleTopDown
{
    using System.Collections;
    using System.Collections.Generic;
    using Virtues;
    using System.Linq;
    using UniRx;
    using UnityEngine;

    public class LifeHUD : MonoBehaviour
    {
        // [SerializeField]Float life;
        [SerializeField]Life life;
        List<Transform> children = new List<Transform>();
        
        void Start()
        {
            foreach(Transform child in this.transform)children.Add(child);
            this.life.CurrentLife.Observable.Subscribe(newValue=>
            {
                children.ForEach(child=>child.gameObject.SetActive(false));
                children.Take((int)newValue).ToList().ForEach(child_=>child_.gameObject.SetActive(true));
            });
        }

        // Update is called once per frame
        void Update()
        {
            
        }
    }

}