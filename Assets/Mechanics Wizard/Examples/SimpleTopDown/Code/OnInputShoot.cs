﻿namespace MechanicsWizard.SimpleTopDown
{
    using System;
    using UnityEngine;
    using UnityEngine.InputSystem;
    using UnityEngine.Events;
    using Virtues;


    // [RequireComponent(PlayerMovement)]
    public class OnInputShoot : ShooterBehaviour
    { 

        public void InputShoot(InputAction.CallbackContext context)
        {
            if(context.action.triggered)
            {
                
                this.Shoot();
            }
        }
    }

}