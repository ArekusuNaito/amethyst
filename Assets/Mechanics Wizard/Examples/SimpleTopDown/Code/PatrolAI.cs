﻿using System;
namespace MechanicsWizard.SimpleTopDown
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Events;
    using Virtues;
    using UniRx;
    using UniRx.Triggers;
    using Pathfinding;

    interface Attacker
    {
        void Attack();
    }
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
public class PatrolAI : MonoBehaviour,Facing2D,Knockbackable
{
    //Pathfinding data
    public bool canResetPosition;
    string targetTag = "Player";
    public LayerMask targetLayer;
    Seeker seeker;
    Transform target;
    Path currentPath;
    int targetNodeIndex=0;
    //
    public LayerMask collisionMask;
    public float patrolRadius=2.5f;
    public float visionRadius=2.5f;
    public float patrolSpeed=2;
    public float chaseSpeed=4;
    Rigidbody2D body;
    new Collider2D collider;
    CircleCollider2D visionCollider;
    public float patrolTime = 2;
    public float idleTime = 1;
    public float hurtTime = 0.4f;
    public float timeToRecalculatePath=0.25f;
    public float attackRate = 1f;
    /// <summary>
    /// Shared timer to use between states
    /// </summary>
    float stateTimer;
    Vector2 startingPosition;
    Vector2 lastDirection;
    private enum State {Idle,Patrol,Chasing,ResetPosition,Hurt,Attack}
    [SerializeField] State state;
    State stateBeforeHurt = State.Idle;
    Dictionary<State,Action> stateMap = new Dictionary<State, Action>();
    //Remind yourself to create the actual "StateMap" class. That way you will be able to send the "target" to the enemy
    public Collision2DEvent onCollision;
    //Interfaces
    public UnityEvent onAttack;
    Vector2 facing = Vector2.zero;
    //INTERFACE IMPLEMENTATION
    public Vector2 Facing{get{return this.facing;}}
    public float KnockbackDuration{get;}
    private IDisposable attackObservable;
    

    void Awake()
    {
        
        this.body = GetComponent<Rigidbody2D>();
        this.collider = GetComponent<Collider2D>();
        this.seeker = GetComponent<Seeker>();
        this.startingPosition = this.transform.position;
        CreateVisionCollider();
        CreateStateMap();
        AddOnCollisionEnter();
        
        
    }

    void AddOnCollisionEnter()
    {
        this.collider.OnCollisionEnter2DAsObservable().Subscribe(other=>
        {
            if(other.CollidesWithMask(this.targetLayer))
            {
                this.onCollision.Invoke(other);
            }
            
        }).AddTo(this);
    }
    void Start()
    {
        this.GoTo(State.Idle);
    }

    void CreateVisionCollider()
    {
        this.visionCollider = Kernel.Create<CircleCollider2D>("Vision Collider");
        visionCollider.isTrigger=true;
        visionCollider.radius = visionRadius;
        visionCollider.transform.SetParent(this.transform,false);
        visionCollider.OnTriggerEnter2DAsObservable().Subscribe(other=>
        {
            if(other.CollidesWithMask(this.targetLayer) && this.state!=State.Chasing)
            {
                this.target=other.transform;
                GoTo(State.Chasing);
                // Debug.Break();
            }
        }).AddTo(this);
    }

    void GoTo(State state)
    {
        this.stateMap[state]();
    }
    void CreateStateMap()
    {
        float randomOffset=0.25f;
        //
        this.stateMap[State.Idle] = ()=>
        {
            // this.currentPath=null;
            this.state=State.Idle;
            stateTimer=Random.Range(idleTime-randomOffset,idleTime+randomOffset);
            this.body.velocity = Vector2.zero;
        };
        this.stateMap[State.Patrol]= ()=>
        {
            this.state=State.Patrol;
            stateTimer=Random.Range(patrolTime-randomOffset,patrolTime+randomOffset);
            // this.body.velocity = PickPatrolDirection()*moveSpeed;
            this.body.AddForce(PickPatrolDirection()*patrolSpeed,ForceMode2D.Impulse);
        };
        this.stateMap[State.ResetPosition]= ()=>
        {
            this.state=State.ResetPosition;
            CreatePathTo(this.startingPosition);
            
        };
        this.stateMap[State.Chasing]=()=>
        {
            this.state = State.Chasing;
            this.body.velocity = Vector2.zero;
            stateTimer=timeToRecalculatePath;
            CreatePathTo(this.target.position); //ToPlaye
            //First time you chase, you will engage attacks.
            //Don't know if I like this overall. As the Attack functionality
            //could be maybe in another script? So far it works!
            if(attackObservable==null)
            {
                attackObservable=Observable.Interval(TimeSpan.FromSeconds(attackRate)).Subscribe(_=>
                {
                    this.onAttack.Invoke();
                }).AddTo(this);
            }
            
        };
        this.stateMap[State.Hurt]=()=>
        {
            this.state = State.Hurt;            
            stateTimer=hurtTime;
        };
        this.stateMap[State.Attack]=()=>
        {
            this.state = State.Attack;            
            // stateTimer=hurtTime;
            // this.target?.GetComponent<Life>()?.Damage(1);
        };

    }

    void CreatePathTo(Vector2 target)
    {
        if(this.seeker.IsDone())
        {
            this.seeker.StartPath(this.body.position,target,pathToOrigin=>
            {
                if(!pathToOrigin.error)
                {
                    this.currentPath=pathToOrigin;
                    this.targetNodeIndex=0;
                    // Debug.Break();
                }else
                {
                    this.currentPath=null;
                    Debug.LogError("Path is null on create");
                } 
            });
        }
    }

    // Update is called once per frame
    void WhenStateTimeEnds(Action onTimeEnd)
    {
        stateTimer-=Time.deltaTime;
        if(stateTimer<=0)
        {
            onTimeEnd();
        }
    }
    void Update()
    {
        if(state!=State.ResetPosition)
        {
            WhenStateTimeEnds(()=>
            {
                if(this.state==State.Idle)
                {                    
                    if(canResetPosition && !IsInStartingArea())
                    {
                        this.GoTo(State.ResetPosition);
                    }
                    else
                    {
                        this.GoTo(State.Patrol);
                    }
                }
                else if(this.state==State.Chasing)
                {
                    // Debug.Log($"Updating Path To Player: {this.target.position}");
                    //Based on what I currently have, it seems that when chase timer ends, you go from Chase->Chase; doesn't sound crazy
                    if(this.target==null)
                    {
                        // Debug.Log("TimerEnds~TargetIsNull");
                        GoTo(State.Idle);
                        return;
                    }
                    GoTo(State.Chasing); //This way you will recalculate
                }
                else if(this.state==State.Hurt)
                {
                    GoTo(stateBeforeHurt);
                }
                else
                {
                    this.GoTo(State.Idle);
                }
            });
        }
        else //Enemy moves back to startingposition
        {
            this.MoveToTarget(onPathComplete:()=>GoTo(State.Idle));
        }
        //Always chase; if you are in chasing mode.
        if(state==State.Chasing)this.MoveToTarget(onPathComplete:()=>GoTo(State.Idle));

        Vector2 clampedVelocity = this.body.velocity;
        clampedVelocity.x = Mathf.Clamp(this.body.velocity.x,-chaseSpeed,chaseSpeed);
        clampedVelocity.y = Mathf.Clamp(this.body.velocity.y,-chaseSpeed,chaseSpeed);
        this.body.velocity=clampedVelocity;
    }

    void MoveToTarget(Action onPathComplete=null)
    {
        if(!this.seeker.IsDone())return; //"Yield" if the pathfinder hasn't finished the pathfinding
        if(this.currentPath==null)
        {
            GoTo(State.Idle);
        }
        else //we have a path and the seeker is done; good to go
        {   
            //Are we on the endNode?
            if(targetNodeIndex>=this.currentPath.vectorPath.Count)
            {
                this.currentPath=null;
                if(onPathComplete!=null)onPathComplete();
                return;
            }
            //Otherwise, we are still on our way to our destination
            // if(targetNodeIndex==0)
            // {
            //     targetNodeIndex++;
            //     return;
            // }
            // var nextNodePosition = (Vector2)this.currentPath.vectorPath[this.targetNodeIndex+1];
            // var distanceToNextNode = Vector2.Distance(this.body.position,nextNodePosition);

            var targetNodePosition = (Vector2)this.currentPath.vectorPath[this.targetNodeIndex];
            var directionToReachNode = (targetNodePosition-this.body.position).normalized;
            var distanceToTargetNode = Vector2.Distance(this.body.position,targetNodePosition);
            
            // Debug.Log($"Distance to next node [{this.targetNodeIndex+1}]: {distanceToNextNode}, [{this.targetNodeIndex}]{distanceToTargetNode}");
            // Debug.Break();
            //Distance to next node [1]: 0.188192, [0]4.768372E-07
            // if(distanceToNextNode<=distanceToTargetNode)
            if(.25f>=distanceToTargetNode) //Node size =0.25f; still looking for a more efficent way to work this out
            {
                targetNodeIndex++;
            }
            this.facing = directionToReachNode;
            this.body.velocity= directionToReachNode*chaseSpeed;
            // this.body.AddForce(directionToReachNode*moveSpeed,ForceMode2D.Impulse);
        }
    }

    public void Knockback(Vector3 direction)
    {
        Debug.Log($"Knockback {this.name}");
        // this.body.velocity = patrolSpeed*2;
        if(this.state!=State.Hurt)stateBeforeHurt=this.state;
        GoTo(State.Hurt);
    }

    Transform FindPlayerInStartingArea()
    {
        var areaCollisions = Physics2D.OverlapCircleAll(startingPosition,patrolRadius).ToList();
        if(areaCollisions.Count>0) //any collision? well, lets see if "I", the AI is one of those
        {
            return areaCollisions.Where(collider=>collider.tag=="Player").SingleOrDefault()?.transform;
        }
        else return null;
    }

    bool IsInStartingArea()
    {
        var areaCollisions = Physics2D.OverlapCircleAll(startingPosition,patrolRadius).ToList();
        if(areaCollisions.Count>0) //any collision? well, lets see if "I", the AI is one of those
        {
            return areaCollisions.Any(collider=>collider==this.collider);
        }
        else return false;
    }

    Vector2 PickPatrolDirection()
    {
        Vector2 randomDirection;
        const int retryTimes = 10;
        var patrolDistanceSafeguard=0.5f; //Unless it's used a lot, i won't put it in the inspector. Feel free to add it as a public value if needed
        for(var retryIndex=0;retryIndex<retryTimes;retryIndex++)
        {
            randomDirection = CardinalPoints.GetRandom8Direction();
            var patrolDistance = this.patrolTime*(this.patrolSpeed*randomDirection.magnitude); //d=v*t
            //Prediction: Is my collider going to collider with something in patrolDistance
            var hit = Physics2D.Raycast(this.transform.position,randomDirection,patrolDistance+patrolDistanceSafeguard,collisionMask);
            if(hit) 
            {
                continue;   
            }
            else //didn't hit something? Then we can use this
            {
                return randomDirection; //Good to go, that direction is good.
            }
        }
        //If we don't get a direction in "retryTimes". Then let's just not move
        return Vector2.zero;
        
    }


}

}