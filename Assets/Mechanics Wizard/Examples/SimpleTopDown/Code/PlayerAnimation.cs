﻿namespace MechanicsWizard.SimpleTopDown
{
    using Virtues;
    using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerMovement))]
public class PlayerAnimation : MonoBehaviour
{
    // Start is called before the first frame update
    
    Facing2D facing2D;
    Animator animator;
    
    // Update is called once per frame
    void Awake()
    {
        this.facing2D = GetComponent<Facing2D>();
        this.animator = GetComponent<Animator>();
    }
    void Update()
    {
        
        this.animator.SetInteger("Facing",CardinalPoints.VectorToEnumInt(facing2D.Facing));
        //To reuse the same animation, but just make it faster/slower if there's no input
        if(facing2D.Facing==Vector2.zero)
        {
                this.animator.speed = 0.5f;
        }else this.animator.speed = 1;
        
    }

}

}