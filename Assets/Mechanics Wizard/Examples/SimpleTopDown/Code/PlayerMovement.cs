﻿namespace MechanicsWizard.SimpleTopDown
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.InputSystem;
    using UniRx;
    using Virtues;

    public class PlayerMovement : MonoBehaviour,Knockbackable,Facing2D
    {
        Vector2 facing = CardinalPoints.Down.vector;

        [SerializeField]float speed = 1f;
        Vector2 leftStick = Vector2.zero;
        Rigidbody2D body;
        [SerializeField]float knockBackDuration = 0.4f;
        public bool onKnockback = false;
        public float knockBackFactor=2;
        
        public Vector2 Facing
        {
            get{return this.facing;}
        }
        public float KnockbackDuration{get{return this.knockBackDuration;}}

        public bool DetectedInput
        {
            get{return this.InputValue != Vector2.zero;}
        }
        public Vector2 CurrentSpeed
        {
            get{return this.leftStick*speed;}
        }
        public Vector2 InputValue
        {
            get{return this.leftStick;}
        }

        void OnEnable()
        {
            // this.input.Enable();
        }

        void OnDisable()
        {
            // this.input.Disable();
        }

        void Awake()
        {
            this.body = GetComponent<Rigidbody2D>();
        }

        public void OnInput(InputAction.CallbackContext context)
        {
            if(!onKnockback)
            {
                this.leftStick = context.ReadValue<Vector2>();
                DetectFacing();
            }
        }

        void DetectFacing()
        {
            var angle = Vector2.SignedAngle(leftStick,Vector2.up); //Use of Vector.up to make the right side positive (0,180) and the left (-180,0)
            angle = CardinalPoints.From180AngleTo360AngleInDegrees(angle);
        
            var newFacing = CardinalPoints.DegreeAngleTo4Direction(angle).vector;

            if(newFacing != this.facing && this.DetectedInput)
            {
                this.facing = newFacing;
            }
        }

        void FixedUpdate()
        {
            if(!onKnockback)
            {
                this.body.velocity = leftStick*speed;
            }
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="myCollision">The Collision Registered from the other object. This holds data of me, the player</param>
        public void Knockback(Collision2D myCollision)
        {
            if(myCollision.contactCount==0)return;
            if(!onKnockback)
            {
                //Turn off collisions between the both of us during this time
                var otherLayer = myCollision.otherCollider.gameObject.layer;
                Physics2D.IgnoreLayerCollision(this.gameObject.layer,otherLayer,ignore:true);
                var myNormal = myCollision.GetContact(0).normal;
                var knockBackDirection = (myNormal*-1).normalized;
                this.onKnockback=true;
                this.leftStick = Vector2.zero;       
                this.body.AddForce(knockBackDirection*(knockBackFactor/knockBackDuration),ForceMode2D.Impulse);
                Observable.Timer(System.TimeSpan.FromSeconds(knockBackDuration))
                .Subscribe(_=>
                {
                    Physics2D.IgnoreLayerCollision(this.gameObject.layer,otherLayer,ignore:false);
                    this.onKnockback=false;
                }).AddTo(this);
            }
        }

        public void Knockback(Vector3 direction)
        {
            
            if(!onKnockback)
            {

                this.onKnockback=true;
                this.leftStick = Vector2.zero;       
                this.body.AddForce(direction.normalized*(knockBackFactor/knockBackDuration),ForceMode2D.Impulse);
                Observable.Timer(System.TimeSpan.FromSeconds(knockBackDuration))
                .Subscribe(_=>
                {
                    this.onKnockback=false;
                }).AddTo(this);
            }
        }
    }

}