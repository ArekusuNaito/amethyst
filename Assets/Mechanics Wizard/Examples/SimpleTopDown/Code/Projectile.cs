﻿namespace MechanicsWizard.SimpleTopDown
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using System;
    using Virtues;
    [RequireComponent(typeof(Rigidbody2D))]
    
    public class Projectile : MonoBehaviour
    {
        // Start is called before the first frame update
        public float power=1;
        Rigidbody2D body;
        new Collider2D collider;
        [SerializeField]bool doesRotateOnSpawn=true;
        Vector2 direction;
        public LayerMask targetMask;
        public LayerMask destroyInThisLayer;
        public float lifeSpan = 6f;
        [HideInInspector]public Collider2DEvent onTriggerEnter;
        public bool didHit;
        
        void Awake()
        {
            this.collider = GetComponent<Collider2D>();
            this.body = GetComponent<Rigidbody2D>();
            Destroy(this.gameObject,lifeSpan); 
        }

        public void Initialize(Vector2 direction,float speed=1,float power=1)
        {
            this.direction = direction;
            if(doesRotateOnSpawn)this.transform.SetRotationZ(Vector2.SignedAngle(Vector2.up,direction));
            this.power=power;
            this.body.velocity = this.direction*speed;
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if(other.CollidesWithMask(targetMask))
            {
                if(!didHit)
                {
                    didHit=true;
                    this.onTriggerEnter.Invoke(other);
                }
            }
            // var life = other.GetComponent<Life>();
            // if(life!=null && life.enabled)
            // {
            //    life.Damage(this.power,this.direction.vector);
            // }
            // This was when used a destroyable
            // var destroyable = other.GetComponent<Destroyable>();
            // if(destroyable && destroyable.gameObject!=this.origin)
            // {               
            //     destroyable.Destroy();
            //     Destroy(this.gameObject);
            // }            
            if(other.CollidesWithMask(destroyInThisLayer))
            {
                Destroy(this.gameObject);
            }
            // if(destroyInThisLayer == (destroyInThisLayer | (1 << other.gameObject.layer)))
            // {
            //     Destroy(this.gameObject);
            // }
        }
    }

}