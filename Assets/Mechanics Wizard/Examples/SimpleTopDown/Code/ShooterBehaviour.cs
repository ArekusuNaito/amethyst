﻿namespace MechanicsWizard.SimpleTopDown
{
    using System.Collections;
    using System.Collections.Generic;
    using Virtues;
    using System;
    using UnityEngine;
    using UnityEngine.Events;

    public class ShooterBehaviour : MonoBehaviour
    {
        [SerializeField]protected GameObject projectile;
        [SerializeField]protected float power=1;
        [SerializeField]protected float speed=3;
        [SerializeField]protected UnityEvent onShoot;
        [SerializeField]protected UnityEvent onHit;
        Facing2D facing2D;

        void Awake()
        {
            this.facing2D=this.GetComponent<Facing2D>();
        }
        public virtual void Shoot()
        {
            this.onShoot.Invoke();
            var facing = this.facing2D.Facing;
            var newProjectile = Instantiate(projectile,this.gameObject.transform.position,Quaternion.identity).GetComponent<Projectile>();

            if(newProjectile!=null)
            {
                newProjectile.Initialize(this.facing2D.Facing,this.speed);
                newProjectile.onTriggerEnter.AddListener(otherCollider=>
                {
                    Damageable damageable;
                    Knockbackable knockbackable;
                    if(otherCollider.TryGetComponent<Damageable>(out damageable))
                    {
                        damageable.Damage(newProjectile.power);
                    }
                    if(otherCollider.TryGetComponent<Knockbackable>(out knockbackable))
                    {
                        // otherCollider.
                        knockbackable.Knockback(facing);
                        
                    }
                    onHit.Invoke();
                    
                });
            }
        }

    }

}