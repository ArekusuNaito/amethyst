﻿namespace MechanicsWizard.SimpleTopDown
{
    using UnityEngine;
    using System;
    using UniRx;

    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteFlashAnimation : MonoBehaviour
    {

        public float flashTime=0.5f;
        public float flashIntervalTime=0.1f;
        SpriteRenderer  spriteRenderer;
        public Color flashColor=Color.white;
        public Material flashMaterial;
        Material spriteDefaultMaterial;
        void Awake()
        {
            this.spriteRenderer=GetComponent<SpriteRenderer>();
            this.spriteDefaultMaterial=spriteRenderer.material;
            flashMaterial.color=this.flashColor;
        }
        public void Flash()
        {
            Observable.Timer(TimeSpan.FromTicks(0),TimeSpan.FromMilliseconds(flashIntervalTime)).Take(TimeSpan.FromSeconds(flashTime))
            .DoOnCompleted(()=>
            {
                if(spriteDefaultMaterial)this.spriteRenderer.material = this.spriteDefaultMaterial;
                this.spriteRenderer.enabled=true;
            })
            .Subscribe(period=>
            {
                if(this.flashMaterial!=null) //On/Off between the materials.
                {
                    this.spriteRenderer.material = (period%2==0)?this.flashMaterial:this.spriteDefaultMaterial;
                }
                else //Just turn on/off the renderer
                {
                    this.spriteRenderer.enabled = (period%2==0)?true:false;
                }
                
                
            }).AddTo(this.gameObject);
        }
    }

}