﻿namespace MechanicsWizard.SimpleTopDown
{
    using Virtues;
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Animator))]
    public class TopDownNPCAnimator : MonoBehaviour
    {
        Rigidbody2D body;
        Animator animator;
        void Awake()
        {
            this.body = this.GetComponent<Rigidbody2D>();
            this.animator = this.GetComponent<Animator>();
        }

        //REFERENCE:
        //public enum Points { Up = 0, Left = 1, Down = 2, Right = 3,UpLeft=4,DownLeft=5,DownRight=6,UpRight=7 };
        void Update()
        {
            // var direction = CardinalPoints.DegreeAngleTo4Direction(Vector2.SignedAngle(Vector2.up,this.body.velocity));
            // this.animator.SetInteger("Facing",(int)direction.value);
            var movingVertically = Mathf.Abs(this.body.velocity.y);
            if(movingVertically>0) //Vertical Movement has priority
            {
                if(this.body.velocity.y>0)SetFacingAnimation((int)CardinalPoints.Points.Up);
                else if(this.body.velocity.y<0)SetFacingAnimation((int)CardinalPoints.Points.Down);
            }
            else
            {
                if(this.body.velocity.x>0)SetFacingAnimation((int)CardinalPoints.Points.Right);
                else if(this.body.velocity.x<0)SetFacingAnimation((int)CardinalPoints.Points.Left);
            }
            
        }

        void SetFacingAnimation(int value)
        {
            this.animator.SetInteger("Facing",value);
        }
    }

}