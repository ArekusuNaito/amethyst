﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Virtues.Audio
{
    
    [CreateAssetMenu(fileName = "New Sound Options", menuName = "Virtues/Options/Sound Options")]
    public class SoundOptions : ScriptableObject
    {
        [SerializeField]UnityEngine.Audio.AudioMixer audioMixer;
        [SerializeField]string masterVolumeExposedName = "MasterVolume";
        [SerializeField]string musicVolumeExposedName = "MusicVolume";
        [SerializeField]string sfxVolumeExposedName = "SFXVolume";
        public float MasterVolume
        {
            set
            {
                SetChannelVolume(this.masterVolumeExposedName,value);
            }
            get{
            {
                return GetChannelVolume(this.masterVolumeExposedName);
            }}
        }

        public float MusicVolume
        {
            set
            {
                SetChannelVolume(this.musicVolumeExposedName,value);
            }
            get{
            {
                return GetChannelVolume(this.musicVolumeExposedName);
            }}
        }

        public float SFXVolume
        {
            set
            {
                SetChannelVolume(this.sfxVolumeExposedName,value);
            }
            get{
            {
                return GetChannelVolume(this.sfxVolumeExposedName);
            }}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelName"></param>
        /// <param name="volume">From 0 to 100</param>
        void SetChannelVolume(string channelName,float volume)
        {
            this.SetExposedParameter(channelName,this.GetDecibelVolume(volume));
        }
        /// <summary>
        /// Get the volume in 0 to 1 values. Where 0 is none, and 1 is 100% sound.
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        float GetChannelVolume(string channelName)
        {
            var decibelVolume = GetExposedParameter(channelName);
            float linearVolume = Mathf.Pow(10,(decibelVolume/20)); //Convert decibels to linear value
            // Debug.Log($"Linear: {linearVolume} Decibel:{decibelVolume}");
            //db = log x * 20; => db/20 = log x; 10^(db/20) = x; 
            return linearVolume;
        }

        /// <summary>
        /// Input is float, output is decibel.virtual Which has a log behaviour.
        /// </summary>
        /// <param name="volume">From [0,1]</param>
        /// <returns>A decibel value from 0db to around -35db</returns>
        float GetDecibelVolume(float volume)
        {
            if(volume>0 && volume<=100)
            {
                //(dB) = 20 × log (volume)
                var newDecibelVolume = Mathf.Log(volume,10)*20;
                // Debug.Log($"{volume}={newDecibelVolume}");
                return newDecibelVolume;
            }
            else if(volume<=0)
            {
                return -80; //db
            }
            else throw new System.Exception("Given volume is not between [0,100]");
            
            
        }

        float GetExposedParameter(string exposedParameter)
        {
            float parameterValue = 0;
            this.audioMixer.GetFloat(exposedParameter,out parameterValue);
            return parameterValue;
        }

        void SetExposedParameter(string exposedParameter,float value)
        {
            this.audioMixer.SetFloat(exposedParameter,value);
        }

    }

}