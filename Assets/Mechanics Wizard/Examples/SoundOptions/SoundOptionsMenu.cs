﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
namespace MechanicsWizard
{
    using System.Collections;
    using System.Collections.Generic;
    using Virtues;
    using Virtues.Systems;
    using Virtues.Audio;

public class SoundOptionsMenu : MonoBehaviour
{
    public SaveSystem saveSystem;
    public SoundOptions soundOptions;
    MechanicsInputMap input;
    [Header("Cursor Positions")]
    public Transform cursor;

    [Header("Options")]
    public Slider masterSlider;
    public Slider musicSlider;
    public Slider sfxSlider;
    public UnityEvent onBackToMenu;

    EventSystem UIEventSystem
    {
        get{return EventSystem.current;}
    }

    public void OnSelect(BaseEventData baseEventData)
    {
        // Debug.Log(baseEventData.selectedObject.name);
        var selectedObject = baseEventData.selectedObject.transform;        
        this.cursor.position = new Vector3(cursor.position.x,selectedObject.position.y,this.cursor.position.z);
    }
    void Awake()
    {
        input = new MechanicsInputMap();
        input.Enable();
        this.masterSlider.Select();
        var soundFile = saveSystem.LoadSoundData();
        if(soundFile!=null)
        {
        //     Debug.Log(soundFile);
            masterSlider.value=soundFile.masterVolume;
            musicSlider.value=soundFile.musicVolume;
            sfxSlider.value=soundFile.sfxVolume;
        }
        else Debug.LogError("SoundFile is null");
        

        input.SoundTestMenu.BacktoMenu.performed+= context=>
        {
            saveSystem.SaveSoundData(soundOptions.MasterVolume,soundOptions.MusicVolume,soundOptions.SFXVolume);
            onBackToMenu.Invoke();
        };
    }
}

}