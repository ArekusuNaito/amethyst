﻿using UnityEngine;
namespace MechanicsWizard
{   
    using DG.Tweening;
    
    public class CursorAnimation : MonoBehaviour
    {
        public float animationTime = 1.5f;
        public Tween currentAnimation;

        void Start()
        {
            this.currentAnimation = this.transform.DOLocalMoveX(this.transform.localPosition.x+10,animationTime).SetEase(Ease.InFlash).SetLoops(-1,LoopType.Yoyo).SetUpdate(true);
            
        }

        public void Pause()
        {
            this.currentAnimation.Pause();
        }

        public void Restart()
        {
            this.currentAnimation.Restart();
        }   

        
    }

}