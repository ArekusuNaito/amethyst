﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using UnityEngine.EventSystems;
namespace SoundTest
{   
    using System.Collections;
    using System.Collections.Generic;
    using Virtues.Audio;
    using UniRx;
    using UnityEngine.UI;
    using MechanicsWizard;
    
    
    public class SoundTest : MonoBehaviour
    {
        enum AudioType {Music,SFX}
        public AudioSource musicChannel;
        public AudioSource sfxChannel;
        public AudioCollection musicCollection;
        public AudioCollection sfxCollection;
        public Text musicIndexUI;
        public Text sfxIndexUI;
        IntReactiveProperty musicIndex=new IntReactiveProperty(0);
        IntReactiveProperty sfxIndex=new IntReactiveProperty(0);
        int currentMusicPlaying=-1;
        int currentSFXPlaying=-1;
        [Tooltip("A sprite animation that represents the 2 animations for idle and playing states")]
        public Animator spriteAnimator;
        public MechanicsInputMap input;
        [SerializeField]AudioType audioType = AudioType.Music;
        public UnityEvent backToMenuEvent;


        // public  backToMenuEvent;
        
        public void OnSelect(BaseEventData baseEventData)
        {
            Debug.Log($"Selected: {baseEventData.selectedObject.name}");
            // this.cursor.position = baseEventData.selectedObject.transform.position;
            SwitchAudioType(ref this.audioType);
        }
        void OnEnable()
        {
            this.audioType = AudioType.Music;
            this.input.Enable();
        }

        void OnDisable()
        {
            this.input.Disable();
        }

        void CreateInput()
        {
            input = new MechanicsInputMap();
            input.SoundTestMenu.Select.performed +=  context =>
            {
                CheckSelectInput();
            };
            
            input.SoundTestMenu.NextFile.performed+= context =>
            {
                
                if(this.audioType==AudioType.Music)
                {
                    
                    NextIndex(ref musicIndex,musicCollection);
                }
                else
                {
                    NextIndex(ref sfxIndex,sfxCollection);
                }
            };
            
            input.SoundTestMenu.PreviousFile.performed+= context =>
            {
                if(this.audioType==AudioType.Music)
                {
                    PreviousIndex(ref musicIndex);
                }
                else PreviousIndex(ref sfxIndex);
            };            
            input.SoundTestMenu.BacktoMenu.performed+= context=>
            {
                this.input.Disable();
                backToMenuEvent.Invoke();
            };
        }

        void Awake()
        {
            CreateInput();   
        }

        void NextIndex(ref IntReactiveProperty audioIndex,AudioCollection audioCollection)
        {
            audioIndex.Value++;
            if(audioIndex.Value>=audioCollection.Count)audioIndex.Value=audioCollection.Count-1;
        }

        void PreviousIndex(ref IntReactiveProperty audioIndex)
        {
            
            audioIndex.Value--;if(audioIndex.Value<0)audioIndex.Value=0;
        }

        void SwitchAudioType(ref AudioType audioType)
        {
            if(audioType==AudioType.Music)audioType = AudioType.SFX;
            else audioType=AudioType.Music;
        }

        void Start()
        {
            musicIndex.SubscribeToText(musicIndexUI);
            sfxIndex.SubscribeToText(sfxIndexUI);
        }

        void PlaySong(int index)
        {
            this.musicChannel.clip=this.musicCollection[currentMusicPlaying];
            this.musicChannel.Play();
            spriteAnimator.SetBool("Playing",true);
        }

        void Stop()
        {
            this.musicChannel.Stop();
            this.currentMusicPlaying=-1;
            spriteAnimator.SetBool("Playing",false);
        }

        void CheckSelectInput()
        {
            
            if(this.audioType == AudioType.Music)
            {
                //Options
                //No song selected? Play it by all means.
                //If a song has been selected, but cursor is on a different song, new song will play.
                //If a song has been selected, and cursor is on the same song, song will stop.
                if(currentMusicPlaying==-1 || currentMusicPlaying!=musicIndex.Value)
                {
                    currentMusicPlaying=musicIndex.Value;
                    PlaySong(currentMusicPlaying);
                }
                else if(currentMusicPlaying==musicIndex.Value)
                {
                    Stop();
                }
            }
            else
            {
                    currentSFXPlaying=sfxIndex.Value;
                    this.sfxChannel.PlayOneShot(sfxCollection[currentSFXPlaying]);   
            }



        }
    }

}