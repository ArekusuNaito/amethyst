﻿namespace Invaders
{
    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Virtues;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D body;
    private bool canYouMove=false;
    public AudioClip bulletSFX,enemyDestroySFX;
    public GameObject destroyPrefabAnimation;
    void Start()
    {
        canYouMove=false;
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.body = GetComponent<Rigidbody2D>();
        switch(PresentationManager.stepNumber.Value)
        {
            case 11: step11(); break;
            case 12: step12();break;
            case 13: step13(); break;

            default: step13(); break;
        }
        if(PresentationManager.stepNumber.Value>=19)
        {
            // Virtues.Audio.AudioMixer.SFXChannel.Play(this.bulletSFX);
        }
        
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.transform.HasComponent<Enemy>())
        {
            if (PresentationManager.stepNumber.Value>=13) 
            {
                //Add sfx and vfx here
                //Enemy
                Destroy(collider.gameObject);
                if (PresentationManager.stepNumber.Value >= 19)
                {
                    // Virtues.Audio.AudioMixer.SFXChannel.Play(this.enemyDestroySFX);
                }
                if (PresentationManager.stepNumber.Value >= 22)
                {
                    var destroyAnimation = Instantiate(destroyPrefabAnimation,collider.transform.position,Quaternion.identity);
                    Virtues.Services.CameraService.Shake(0.3f);
                    Destroy(destroyAnimation,0.45f);
                }
                
                
                //Bullet
                Destroy(this.gameObject);
            }
            if(PresentationManager.stepNumber.Value>=15)
            {
                PresentationManager.score.Value+=10;
            }
        }
    }

    void step11()
    {
        canYouMove=false;
    }
    // Update is called once per frame
    void step12()
    {
        canYouMove=true;
        this.body.velocity = new Vector2(0,4);
    }

    void step13()
    {
        step12();
    }
}

}