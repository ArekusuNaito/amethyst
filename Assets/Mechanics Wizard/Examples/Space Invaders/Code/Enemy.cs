﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    private SpriteRenderer spriteRenderer;
    public Sprite uglyGraphic;
    public Sprite awesomeGraphic;
    
    private bool canYouMove=false;
    private bool canYouCollide=false;
    private bool canGoDown=false;
    public float speed = 2;
    private Rigidbody2D body;
    void Awake()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.body = GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Debug.Log("Paso algo");
        if(canYouCollide)
        {
            if(collision.gameObject.layer==9)//9 = walls
            {
                this.speed = -this.speed;
                if (canGoDown)
                {
                    var enemyPosition = this.transform.position;
                    enemyPosition.y -= 1;
                    this.transform.position = enemyPosition;
                }
            }
            
        }
        else
        {

        }
    }

    // Update is called once per frame
    void Update()
    {
        this.body.velocity = Vector2.zero;
        if(canYouMove)
        {
            this.body.velocity = new Vector2(this.speed,0);
        }
    }

    public void step1()
    {
        this.gameObject.SetActive(false);
    }

    public void step2()
    {
        this.gameObject.SetActive(true);
    }
    public void step3()
    {
        this.spriteRenderer.color = Color.white;
    }
    public void step4()
    {
        this.spriteRenderer.color = Color.red;
    }

    public void step7()
    {
        step2();//visible
        step4(); //color
        canYouMove=true;
        canYouCollide=false;
        
    }

    public void step8()
    {
        step7();
        this.speed=10;
        canYouCollide=true;
        canGoDown=false;
    }

    public void step9()
    {
        step8();
        canGoDown=true;
        speed=30;
    }

    public void step10()
    {
        step9();
        speed=25;
    }

    public void step11()
    {
        step9();
        speed=9;
    }

    public void step16()
    {
        step11();
        this.spriteRenderer.color = Color.white;
        this.spriteRenderer.sprite = awesomeGraphic;
    }
}
