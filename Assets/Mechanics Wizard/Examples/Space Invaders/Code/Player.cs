﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Invaders
{

using UniRx;
using Virtues;
public class Player : MonoBehaviour
{
    public Sprite uglyGraphic;
    public Sprite awesomeGraphic;
    public float speed=5;
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D body;
    public bool canYouMove=false;
    private bool canYouGetKilledByEnemies=false;
    private bool canYouShoot=false;
    public GameObject bulletPrefab;
    private Vector3 startingPosition;
    // Start is called before the first frame update
    void Awake()
    {

        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.body=GetComponent<Rigidbody2D>();
    }
    public void SaveStartingPosition()
    {
        this.startingPosition = this.transform.position;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.HasComponent<Enemy>())
        {
            if(canYouGetKilledByEnemies)this.gameObject.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        body.velocity=Vector2.zero;
        if(canYouMove)
        {
            var leftStick = Virtues.Input.Gamepad.LeftStick;
            body.velocity = new Vector2(leftStick.x *this.speed,0);
        }
        if(canYouShoot)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                var bullet = Instantiate(bulletPrefab,GetComponent<BoxCollider2D>().bounds.CenterUp(),Quaternion.identity);
            }
        }
    }

    public void ResetPosition()
    {
        this.transform.position = startingPosition;
    }
    public void step1()
    {
        this.canYouMove=false;
        this.gameObject.SetActive(true);
        this.spriteRenderer.color = Color.white;
    }
    public void step2()
    {
        step1();
    }

    public void step3()
    {
        this.spriteRenderer.color = Color.green;
    }
    public void step4()
    {
        this.canYouMove=false;
    }

    public void step5()
    {
        this.canYouMove=true;
        this.body.bodyType = RigidbodyType2D.Kinematic;
        this.speed=15;
    }

    public void step6()
    {
        this.gameObject.SetActive(true);
        this.body.bodyType = RigidbodyType2D.Dynamic;
        this.canYouMove = true;
        this.speed = 5;
    }
    public void step9()
    {
        step6();
        this.gameObject.SetActive(true);
        canYouGetKilledByEnemies=false;
        this.body.bodyType = RigidbodyType2D.Kinematic;
    }

    public void step10() //can be killed
    {
        step9();
        canYouGetKilledByEnemies=true;
        canYouShoot=false;
        this.body.bodyType = RigidbodyType2D.Dynamic;
        this.gameObject.SetActive(true);
    }

    public void step11()
    {
        step10();
        canYouShoot=true;
    }

    public void step16()
    {
        step11();
        this.spriteRenderer.color = Color.green;
        this.spriteRenderer.sprite = uglyGraphic;
    }
    public void step17()
    {
        this.spriteRenderer.color = Color.white;
        this.spriteRenderer.sprite = awesomeGraphic;
    }
    
}

}