﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Virtues;
namespace Invaders
{
public class PresentationManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static IntReactiveProperty stepNumber = new IntReactiveProperty(0);
    public static IntReactiveProperty score = new IntReactiveProperty(0);
    public Text textStepNumber;
    [Header("Player")]
    public Player player;
    [Header("Enemies")]
    public int horizontalCount=5;
    public int verticalCount=5;
    public GameObject initialCoordinate;
    public GameObject enemy;
    public GameObject enemyContainer;

    [Header("Score")]
    public Text scoreText;
    [Header("Background")]
    public GameObject background;

    [Header("Audio")]
    public AudioClip unfittingTheme;
    public AudioClip fittingTheme;
    void Start()
    {
        stepNumber.SubscribeToText(textStepNumber);
        score.SubscribeToText(scoreText,(jiji)=>$"SCORE: {jiji}");
        this.player.gameObject.SetActive(false);
        
        player.SaveStartingPosition();
        enemyContainer.transform.DestroyChildren();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            PreviousStep();
        }   
        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            NextStep();
        }
    }

    void NextStep()
    {
        stepNumber.Value++;
        SetStep(stepNumber.Value);
    }
    void PreviousStep()
    {
        if(stepNumber.Value<=0)
        {
            
            return;
        }
        
        stepNumber.Value--;
        SetStep(stepNumber.Value);
    }

    void SetStep(int stepNumber)
    {
        this.player.ResetPosition();
        DestroyAllBullets();
        score.Value=0;
        switch(stepNumber)
        {
            case 0: player.gameObject.SetActive(false);
                this.enemyContainer.transform.DestroyChildren();
                break;
            case 1: step1();break;
            case 2: step2(); break;
            case 3: step3(); break;
            case 4: step4(); break;
            case 5: step5(); break;
            case 6: step6(); break;
            case 7: step7(); break;
            case 8: step8(); break;
            case 9: step9(); break;
            case 10: step10(); break;
            case 11: step11(); break;
            case 12: step12(); break;
            case 13: step13(); break;
            case 14: step14(); break;
            case 15: step15(); break;
            case 16: step16(); break;
            //all set, time to make stuff pretty
            case 17: step17(); break;
            case 18: step18(); break;
            case 19: step19(); break;
            case 20: step20(); break;
            case 21: step21(); break;
            case 22: step22(); break;
            case 23: step23(); break;

        }
    }

    void step1() //Player alone, white color
    {  
        player.step1();
        enemyContainer.transform.DestroyChildren();
    }

    void step2()
    {
        this.player.step2();
        CreateEnemies();
        
    }

    void step3()
    {
        this.player.step3();
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone=>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step3();
        });
    }

    void step4()
    {
        this.player.step4();
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step4();
        });
    }

    void step5()
    {
        player.step5();
    }

    void step6()
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step4();
        });
        player.step6();
    }

    void step7()
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step7();
        });
    }
    void step8()
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step8();
        });
    }


    void step9()
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step9();
        });
    }

    void step10() //kill player
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step10();
        });
        player.step10();
    }

    void step11() //player can shoot
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step11();
        });
        player.step11();

    }

    void step12() //bullets can movep
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step11();
        });
        player.step11();
        this.scoreText.gameObject.SetActive(false);
        
    }

    void step13() //bullets get destroyed on collision
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step11();
        });
        player.step11();
        this.scoreText.gameObject.SetActive(false);
    }

    void step14() //Show score label
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step11();
        });
        player.step11();
        this.scoreText.gameObject.SetActive(true);
    }
    void step15() //add score on kill
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step11();
        });
        player.step11();
    }
    void step16() //make pretty enemies
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step16(); 
        });
        player.step16();
    }

    void step17()  // make a pretty player
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step16();
        });
        player.step17();
        this.background.SetActive(false);
    }
    void step18() //show up background
    {
        this.enemyContainer.transform.DestroyChildren();
        CreateEnemies(enemyClone =>
        {
            var enemy = enemyClone.GetComponent<Enemy>();
            enemy.step16();
        });
        player.step17();
        this.background.SetActive(true);
    }
    void step19() // add sound effects
    {
        step18();
        // Virtues.Audio.AudioMixer.MusicChannel.Stop();
    }
    void step20() // add music that doesnt fit
    {
        step18();
        // Virtues.Audio.AudioMixer.MusicChannel.Play(this.unfittingTheme);
    }

    void step21() // add music that fits
    {
        this.horizontalCount = 3;
        step18();
        player.speed=8;

        // Virtues.Audio.AudioMixer.MusicChannel.Play(this.fittingTheme);
    }

    void step22() //add a death animation
    {
        this.horizontalCount=8;
        step18();
    }
    void step23()
    {

    }

    void DestroyAllBullets()
    {
        var bullets = GameObject.FindObjectsOfType<Bullet>();
        foreach (var bullet in bullets)
        {
            Destroy(bullet.gameObject);
        }
    }



    void CreateEnemies(System.Action<GameObject> step=null)
    {
        enemyContainer.transform.DestroyChildren();
        for(var horizontalIndex=0;horizontalIndex<horizontalCount*2;horizontalIndex+=2)
        {
            var enemyPosition = initialCoordinate.transform.position;
            enemyPosition.x+=horizontalIndex;
            var enemyClone = Instantiate(enemy,enemyPosition,Quaternion.identity);
            enemyClone.transform.SetParent(enemyContainer.transform);
            enemyClone.SetActive(true);
            if(step!=null)step(enemyClone);
        }
    }
}

}
