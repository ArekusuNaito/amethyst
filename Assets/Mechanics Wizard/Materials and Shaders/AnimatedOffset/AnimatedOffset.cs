﻿namespace Virtues.Shaders
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine.UI;
    using UnityEngine;
    using UniRx;

    public class AnimatedOffset : MonoBehaviour
    {
        
        [SerializeField] Material material = null;
        public Vector2ReactiveProperty offset=new Vector2ReactiveProperty(new Vector2(1,1));
        public Vector2ReactiveProperty tiling = new Vector2ReactiveProperty(new Vector2(1,1));
        Material materialToApply;
        void Awake()
        {
            
            ForceMaterial();
            
        }


        void ForceMaterial()
        {
            
            if(this.gameObject.HasComponent<Image>())
            {
                var image = GetComponent<Image>();
                image.material = material;
                this.materialToApply = image.material;
                
            }
            else
            {
                
                var renderer = GetComponent<Renderer>();
                renderer.material = this.material;
                this.materialToApply = renderer.material;
                // offset.Subscribe(newValues=>materialToApply.SetVector("_Offset",newValues));
                // tiling.Subscribe(newValues=>materialToApply.SetVector("_Tiling",newValues));
            }
            offset.Subscribe(newValues=>materialToApply.SetVector("_Offset",newValues));
            tiling.Subscribe(newValues=>materialToApply.SetVector("_Tiling",newValues));
            
            
        }
    }

}